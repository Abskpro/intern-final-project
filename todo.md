#Features

1. User registration
2. Login
3. Home page
4. Product search
5. Product List
6. Lazy loading in product list (pagination)
7. Product detail page.
8. Add to cart and wishlist
9. Cart page (Also calculate amount of the items in cart)

#Screens required

1. Login
2. Registration
3. HomeScreen
4. Product Detail Screens
5. Cart Screens
6. Sidebar

# Widgets

1. SearchBar
2. BottomNavigationBar
3. Number Input
4. Password Input
5. Rounded Button
6. DateInput
7. toggle Button
8. Checkbox
9.
