// To parse this JSON data, do
//
//     final banner = bannerFromJson(jsonString);

import 'dart:convert';

class Banners {
  Banners({
    this.active,
    this.target,
    this.type,
    this.screens,
    this.displayOrder,
    this.category,
    this.actionUrl,
    this.title,
    this.image,
    this.description,
    this.id,
  });

  bool active;
  Target target;
  String type;
  List<dynamic> screens;
  int displayOrder;
  String category;
  String actionUrl;
  String title;
  String image;
  String description;
  String id;

  factory Banners.fromRawJson(String str) => Banners.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Banners.fromJson(Map<String, dynamic> json) => Banners(
        active: json["active"],
        target: Target.fromJson(json["target"]),
        type: json["type"],
        screens: List<dynamic>.from(json["screens"].map((x) => x)),
        displayOrder: json["display_order"],
        category: json["category"],
        actionUrl: json["action_url"],
        title: json["title"],
        image: json["image"],
        description: json["description"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "active": active,
        "target": target.toJson(),
        "type": type,
        "screens": List<dynamic>.from(screens.map((x) => x)),
        "display_order": displayOrder,
        "category": category,
        "action_url": actionUrl,
        "title": title,
        "image": image,
        "description": description,
        "id": id,
      };
}

class Target {
  Target({
    this.age,
  });

  String age;

  factory Target.fromRawJson(String str) => Target.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Target.fromJson(Map<String, dynamic> json) => Target(
        age: json["age"],
      );

  Map<String, dynamic> toJson() => {
        "age": age,
      };
}
