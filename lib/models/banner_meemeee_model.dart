class BannerMeemee {
  List<Data> data;

  BannerMeemee({this.data});

  BannerMeemee.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String id;
  String name;
  String comment;
  Images images;
  bool isFeatured;

  Data({this.id, this.name, this.comment, this.images, this.isFeatured});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    comment = json['comment'];
    images =
        json['images'] != null ? new Images.fromJson(json['images']) : null;
    isFeatured = json['is_featured'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['comment'] = this.comment;
    if (this.images != null) {
      data['images'] = this.images.toJson();
    }
    data['is_featured'] = this.isFeatured;
    return data;
  }
}

class Images {
  String s128;
  String s256;
  String s512;
  String s1024;
  String s1920;

  Images({this.s128, this.s256, this.s512, this.s1024, this.s1920});

  Images.fromJson(Map<String, dynamic> json) {
    s128 = json['128'];
    s256 = json['256'];
    s512 = json['512'];
    s1024 = json['1024'];
    s1920 = json['1920'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['128'] = this.s128;
    data['256'] = this.s256;
    data['512'] = this.s512;
    data['1024'] = this.s1024;
    data['1920'] = this.s1920;
    return data;
  }
}
