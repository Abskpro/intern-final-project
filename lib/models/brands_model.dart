// To parse this JSON data, do
//
//     final brandsBanner = brandsBannerFromJson(jsonString);

import 'dart:convert';

class BrandsBanner {
  BrandsBanner({
    this.page,
    this.totalResults,
    this.totalPages,
    this.results,
    this.hasNext,
    this.hasPrev,
    this.data,
  });

  int page;
  int totalResults;
  int totalPages;
  List<dynamic> results;
  bool hasNext;
  bool hasPrev;
  List<Datum> data;

  factory BrandsBanner.fromRawJson(String str) =>
      BrandsBanner.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BrandsBanner.fromJson(Map<String, dynamic> json) => BrandsBanner(
        page: json["page"],
        totalResults: json["total_results"],
        totalPages: json["total_pages"],
        results: List<dynamic>.from(json["results"].map((x) => x)),
        hasNext: json["has_next"],
        hasPrev: json["has_prev"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "total_results": totalResults,
        "total_pages": totalPages,
        "results": List<dynamic>.from(results.map((x) => x)),
        "has_next": hasNext,
        "has_prev": hasPrev,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.comment,
    this.images,
    this.isFeatured,
  });

  String id;
  String name;
  String comment;
  Map<String, String> images;
  dynamic isFeatured;

  factory Datum.fromRawJson(String str) => Datum.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        comment: json["comment"],
        images: Map.from(json["images"])
            .map((k, v) => MapEntry<String, String>(k, v)),
        isFeatured: json["is_featured"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "comment": comment,
        "images":
            Map.from(images).map((k, v) => MapEntry<String, dynamic>(k, v)),
        "is_featured": isFeatured,
      };
}
