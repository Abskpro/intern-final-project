import 'dart:convert';

class Login {
  Login({
    this.id,
    this.permissions,
    this.roles,
    this.username,
  });

  String id;
  List<String> permissions;
  String roles;
  String username;

  factory Login.fromRawJson(String str) => Login.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Login.fromJson(Map<String, dynamic> json) => Login(
        id: json["id"],
        permissions: List<String>.from(json["permissions"].map((x) => x)),
        roles: json["roles"],
        username: json["username"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "permissions": List<dynamic>.from(permissions.map((x) => x)),
        "roles": roles,
        "username": username,
      };
}


