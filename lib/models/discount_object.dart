class DiscountObject {
	int id;
	String type;
	int discount;
	int minQuantity;
	int minPurchaseValue;
	int sellingPriceThreshold;

	DiscountObject({
		this.id,
		this.type,
		this.discount,
		this.minQuantity,
		this.minPurchaseValue,
		this.sellingPriceThreshold,
	});

	factory DiscountObject.fromJson(Map<String, dynamic> json) {
		return DiscountObject(
			id: json['id'] as int,
			type: json['type'] as String,
			discount: json['discount'] as int,
			minQuantity: json['min_quantity'] as int,
			minPurchaseValue: json['min_purchase_value'] as int,
			sellingPriceThreshold: json['selling_price_threshold'] as int,
		);
	}

	Map<String, dynamic> toJson() {
		return {
			'id': id,
			'type': type,
			'discount': discount,
			'min_quantity': minQuantity,
			'min_purchase_value': minPurchaseValue,
			'selling_price_threshold': sellingPriceThreshold,
		};
	}
}
