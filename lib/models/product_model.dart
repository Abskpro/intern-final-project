import 'dart:convert';

class ProductMeemee {
  ProductMeemee({
    this.page,
    this.totalResults,
    this.totalPages,
    this.results,
    this.hasNext,
    this.hasPrev,
    this.data,
  });

  // int page;
  // int totalResults;
  // int totalPages;
  var page;
  var totalResults;
  var totalPages;
  List<dynamic> results;
  bool hasNext;
  bool hasPrev;
  List<Datum> data;

  factory ProductMeemee.fromRawJson(String str) =>
      ProductMeemee.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductMeemee.fromJson(Map<String, dynamic> json) => ProductMeemee(
        page: json["page"] == null ? null : json["page"],
        totalResults:
            json["total_results"] == null ? null : json["total_results"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        results: json["results"] == null
            ? null
            : List<dynamic>.from(json["results"].map((x) => x)),
        hasNext: json["has_next"] == null ? null : json["has_next"],
        hasPrev: json["has_prev"] == null ? null : json["has_prev"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page": page == null ? null : page,
        "total_results": totalResults == null ? null : totalResults,
        "total_pages": totalPages == null ? null : totalPages,
        "results":
            results == null ? null : List<dynamic>.from(results.map((x) => x)),
        "has_next": hasNext == null ? null : hasNext,
        "has_prev": hasPrev == null ? null : hasPrev,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.productId,
    this.item,
    this.label,
    this.consumptionSize,
    this.image,
    this.price,
    this.discount,
    this.discountObject,
    this.description,
    this.comment,
    this.otherUnits,
    this.defaultUnit,
    this.soldOut,
    this.manufacturingCompany,
    this.consumptionUnit,
    this.productCategory,
    this.disabled,
    this.offer,
    this.brand,
  });

  String productId;
  String item;
  String label;
  String consumptionSize;
  List<Map<String, String>> image;
  //double price;
  var price;
  // int discount;
  var discount;
  DiscountObject discountObject;
  String description;
  Comment comment;
  List<Unit> otherUnits;
  Unit defaultUnit;
  bool soldOut;
  String manufacturingCompany;
  ConsumptionUnit consumptionUnit;
  ProductCategory productCategory;
  bool disabled;
  Brand offer;
  Brand brand;

  factory Datum.fromRawJson(String str) => Datum.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        productId: json["product_id"] == null ? null : json["product_id"],
        item: json["item"] == null ? null : json["item"],
        label: json["label"] == null ? null : json["label"],
        consumptionSize:
            json["consumption_size"] == null ? null : json["consumption_size"],
        image: json["image"] == null
            ? null
            : List<Map<String, String>>.from(json["image"].map((x) =>
                Map.from(x).map((k, v) => MapEntry<String, String>(k, v)))),
        // price: json["price"] == null ? null : json["price"].toDouble(),
        price: json["price"] == null ? null : json["price"],
        discount: json["discount"] == null ? null : json["discount"],
        discountObject: json["discount_object"] == null
            ? null
            : DiscountObject.fromJson(json["discount_object"]),
        description: json["description"] == null ? null : json["description"],
        comment:
            json["comment"] == null ? null : commentValues.map[json["comment"]],
        otherUnits: json["other_units"] == null
            ? null
            : List<Unit>.from(json["other_units"].map((x) => Unit.fromJson(x))),
        defaultUnit: json["default_unit"] == null
            ? null
            : Unit.fromJson(json["default_unit"]),
        soldOut: json["sold_out"] == null ? null : json["sold_out"],
        manufacturingCompany: json["manufacturing_company"] == null
            ? null
            : json["manufacturing_company"],
        consumptionUnit: json["consumption_unit"] == null
            ? null
            : consumptionUnitValues.map[json["consumption_unit"]],
        productCategory: json["product_category"] == null
            ? null
            : ProductCategory.fromJson(json["product_category"]),
        disabled: json["disabled"] == null ? null : json["disabled"],
        offer: json["offer"] == null ? null : Brand.fromJson(json["offer"]),
        brand: json["brand"] == null ? null : Brand.fromJson(json["brand"]),
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId == null ? null : productId,
        "item": item == null ? null : item,
        "label": label == null ? null : label,
        "consumption_size": consumptionSize == null ? null : consumptionSize,
        "image": image == null
            ? null
            : List<dynamic>.from(image.map((x) =>
                Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))),
        "price": price == null ? null : price,
        "discount": discount == null ? null : discount,
        "discount_object":
            discountObject == null ? null : discountObject.toJson(),
        "description": description == null ? null : description,
        "comment": comment == null ? null : commentValues.reverse[comment],
        "other_units": otherUnits == null
            ? null
            : List<dynamic>.from(otherUnits.map((x) => x.toJson())),
        "default_unit": defaultUnit == null ? null : defaultUnit.toJson(),
        "sold_out": soldOut == null ? null : soldOut,
        "manufacturing_company":
            manufacturingCompany == null ? null : manufacturingCompany,
        "consumption_unit": consumptionUnit == null
            ? null
            : consumptionUnitValues.reverse[consumptionUnit],
        "product_category":
            productCategory == null ? null : productCategory.toJson(),
        "disabled": disabled == null ? null : disabled,
        "offer": offer == null ? null : offer.toJson(),
        "brand": brand == null ? null : brand.toJson(),
      };
}

class Brand {
  Brand();

  factory Brand.fromRawJson(String str) => Brand.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Brand.fromJson(Map<String, dynamic> json) => Brand();

  Map<String, dynamic> toJson() => {};
}

enum Comment {
  A_PACK_OF_A_THERMOMETER,
  EMPTY,
  A_PACK_OF_A_PIECE,
  A_PACK_OF_50_LANCETS_AND_50_TEST_STRIPS
}

final commentValues = EnumValues({
  "A pack of 50 lancets and 50 test strips":
      Comment.A_PACK_OF_50_LANCETS_AND_50_TEST_STRIPS,
  "A pack of a piece.": Comment.A_PACK_OF_A_PIECE,
  "A pack of a thermometer.": Comment.A_PACK_OF_A_THERMOMETER,
  "": Comment.EMPTY
});

enum ConsumptionUnit { PIECE, PACK, BOX, VIAL }

final consumptionUnitValues = EnumValues({
  "BOX": ConsumptionUnit.BOX,
  "PACK": ConsumptionUnit.PACK,
  "PIECE": ConsumptionUnit.PIECE,
  "VIAL": ConsumptionUnit.VIAL
});

class Unit {
  Unit({
    this.id,
    this.name,
    this.unitDefault,
  });

  // int id;
  var id;
  ConsumptionUnit name;
  bool unitDefault;

  factory Unit.fromRawJson(String str) => Unit.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Unit.fromJson(Map<String, dynamic> json) => Unit(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null
            ? null
            : consumptionUnitValues.map[json["name"]],
        unitDefault: json["default"] == null ? null : json["default"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : consumptionUnitValues.reverse[name],
        "default": unitDefault == null ? null : unitDefault,
      };
}

class DiscountObject {
  DiscountObject({
    this.id,
    this.type,
    this.discount,
    this.minQuantity,
    this.minPurchaseValue,
    this.sellingPriceThreshold,
  });

  // int id;
  var id;
  Type type;
  // int discount;
  // int minQuantity;
  // int minPurchaseValue;
  var discount;
  var minQuantity;
  var minPurchaseValue;
  //double sellingPriceThreshold;
  var sellingPriceThreshold;

  factory DiscountObject.fromRawJson(String str) =>
      DiscountObject.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DiscountObject.fromJson(Map<String, dynamic> json) => DiscountObject(
        id: json["id"] == null ? null : json["id"],
        type: json["type"] == null ? null : typeValues.map[json["type"]],
        discount: json["discount"] == null ? null : json["discount"],
        minQuantity: json["min_quantity"] == null ? null : json["min_quantity"],
        minPurchaseValue: json["min_purchase_value"] == null
            ? null
            : json["min_purchase_value"],
        sellingPriceThreshold: json["selling_price_threshold"] == null
            ? null
            // : json["selling_price_threshold"].toDouble(),
            : json["selling_price_threshold"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "type": type == null ? null : typeValues.reverse[type],
        "discount": discount == null ? null : discount,
        "min_quantity": minQuantity == null ? null : minQuantity,
        "min_purchase_value":
            minPurchaseValue == null ? null : minPurchaseValue,
        "selling_price_threshold":
            sellingPriceThreshold == null ? null : sellingPriceThreshold,
      };
}

enum Type { PERCENTAGE }

final typeValues = EnumValues({"percentage": Type.PERCENTAGE});

class ProductCategory {
  ProductCategory({
    this.id,
  });

  String id;

  factory ProductCategory.fromRawJson(String str) =>
      ProductCategory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductCategory.fromJson(Map<String, dynamic> json) =>
      ProductCategory(
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
