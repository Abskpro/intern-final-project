import 'dart:convert';

class UserProfile {
  UserProfile({
    this.address,
    this.bloodGroup,
    this.dateOfBirth,
    this.email,
    this.firstName,
    this.gender,
    this.homePhone,
    this.id,
    this.lastName,
    this.middleName,
    this.mobile,
    this.profileImage,
    this.userId,
  });

  List<dynamic> address;
  String bloodGroup;
  DateTime dateOfBirth;
  dynamic email;
  String firstName;
  String gender;
  dynamic homePhone;
  String id;
  String lastName;
  dynamic middleName;
  String mobile;
  dynamic profileImage;
  String userId;

  factory UserProfile.fromRawJson(String str) =>
      UserProfile.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserProfile.fromJson(Map<String, dynamic> json) => UserProfile(
        address: json["address"] == null
            ? null
            : List<dynamic>.from(json["address"].map((x) => x)),
        bloodGroup: json["blood_group"] == null ? null : json["blood_group"],
        dateOfBirth: json["date_of_birth"] == null
            ? null
            : DateTime.parse(json["date_of_birth"]),
        email: json["email"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        gender: json["gender"] == null ? null : json["gender"],
        homePhone: json["home_phone"],
        id: json["id"] == null ? null : json["id"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        middleName: json["middle_name"],
        mobile: json["mobile"] == null ? null : json["mobile"],
        profileImage: json["profile_image"],
        userId: json["user_id"] == null ? null : json["user_id"],
      );

  Map<String, dynamic> toJson() => {
        "address":
            address == null ? null : List<dynamic>.from(address.map((x) => x)),
        "blood_group": bloodGroup == null ? null : bloodGroup,
        "date_of_birth": dateOfBirth == null
            ? null
            : "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
        "email": email,
        "first_name": firstName == null ? null : firstName,
        "gender": gender == null ? null : gender,
        "home_phone": homePhone,
        "id": id == null ? null : id,
        "last_name": lastName == null ? null : lastName,
        "middle_name": middleName,
        "mobile": mobile == null ? null : mobile,
        "profile_image": profileImage,
        "user_id": userId == null ? null : userId,
      };
}
