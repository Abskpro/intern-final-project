class CartProduct {
  double quantity;
  String productId;
  double unitPrice;
  double discount;
  int unitId;
  String label;
  List<Image> image;
  bool soldOut;
  bool disabled;
  double price;
  String comment;
  String manufacturingCompany;

  CartProduct(
      {this.quantity,
      this.productId,
      this.unitPrice,
      this.discount,
      this.unitId,
      this.label,
      this.image,
      this.soldOut,
      this.disabled,
      this.price,
      this.comment,
      this.manufacturingCompany});

  CartProduct.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'];
    productId = json['product_id'];
    unitPrice = json['unit_price'];
    discount = json['discount'];
    unitId = json['unit_id'];
    label = json['label'];
    if (json['image'] != null) {
      image = new List<Image>();
      json['image'].forEach((v) {
        image.add(new Image.fromJson(v));
      });
    }
    soldOut = json['sold_out'];
    disabled = json['disabled'];
    price = json['price'];
    comment = json['comment'];
    manufacturingCompany = json['manufacturing_company'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quantity'] = this.quantity;
    data['product_id'] = this.productId;
    data['unit_price'] = this.unitPrice;
    data['discount'] = this.discount;
    data['unit_id'] = this.unitId;
    data['label'] = this.label;
    if (this.image != null) {
      data['image'] = this.image.map((v) => v.toJson()).toList();
    }
    data['sold_out'] = this.soldOut;
    data['disabled'] = this.disabled;
    data['price'] = this.price;
    data['comment'] = this.comment;
    data['manufacturing_company'] = this.manufacturingCompany;
    return data;
  }
}

class Image {
  String s128;
  String s256;
  String s512;
  String s1024;
  String s1920;

  Image({this.s128, this.s256, this.s512, this.s1024, this.s1920});

  Image.fromJson(Map<String, dynamic> json) {
    s128 = json['128'];
    s256 = json['256'];
    s512 = json['512'];
    s1024 = json['1024'];
    s1920 = json['1920'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['128'] = this.s128;
    data['256'] = this.s256;
    data['512'] = this.s512;
    data['1024'] = this.s1024;
    data['1920'] = this.s1920;
    return data;
  }
}
