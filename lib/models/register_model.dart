import 'dart:convert';

Register registerFromJson(String str) => Register.fromJson(json.decode(str));

String registerToJson(Register data) => json.encode(data.toJson());

class Register {
  Register({
    this.username,
    this.termsAndConditions,
    this.roles,
    this.paidConsultation,
    this.consultation,
    this.freeConsultation,
    this.dateOfBirth,
    this.createdAt,
    this.doctorsLog,
    this.onlineDoctorLog,
    this.id,
    this.records,
  });

  String username;
  bool termsAndConditions;
  String roles;
  List<dynamic> paidConsultation;
  List<dynamic> consultation;
  List<dynamic> freeConsultation;
  DateTime dateOfBirth;
  DateTime createdAt;
  List<dynamic> doctorsLog;
  List<dynamic> onlineDoctorLog;
  String id;
  List<dynamic> records;

  factory Register.fromJson(Map<String, dynamic> json) => Register(
        username: json["username"],
        termsAndConditions: json["terms_and_conditions"],
        roles: json["roles"],
        paidConsultation:
            List<dynamic>.from(json["paid_consultation"].map((x) => x)),
        consultation: List<dynamic>.from(json["consultation"].map((x) => x)),
        freeConsultation:
            List<dynamic>.from(json["free_consultation"].map((x) => x)),
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        createdAt: DateTime.parse(json["created_at"]),
        doctorsLog: List<dynamic>.from(json["doctors_log"].map((x) => x)),
        onlineDoctorLog:
            List<dynamic>.from(json["online_doctor_log"].map((x) => x)),
        id: json["id"],
        records: List<dynamic>.from(json["records"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "terms_and_conditions": termsAndConditions,
        "roles": roles,
        "paid_consultation": List<dynamic>.from(paidConsultation.map((x) => x)),
        "consultation": List<dynamic>.from(consultation.map((x) => x)),
        "free_consultation": List<dynamic>.from(freeConsultation.map((x) => x)),
        "date_of_birth":
            "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
        "created_at": createdAt.toIso8601String(),
        "doctors_log": List<dynamic>.from(doctorsLog.map((x) => x)),
        "online_doctor_log": List<dynamic>.from(onlineDoctorLog.map((x) => x)),
        "id": id,
        "records": List<dynamic>.from(records.map((x) => x)),
      };
}
