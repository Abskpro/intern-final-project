import 'dart:convert';

class WishList {
  WishList({
    this.page,
    this.totalResults,
    this.totalPages,
    this.results,
    this.hasNext,
    this.hasPrev,
    this.data,
  });

  int page;
  int totalResults;
  int totalPages;
  List<dynamic> results;
  bool hasNext;
  bool hasPrev;
  List<Datum> data;

  factory WishList.fromRawJson(String str) =>
      WishList.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory WishList.fromJson(Map<String, dynamic> json) => WishList(
        page: json["page"] == null ? null : json["page"],
        totalResults:
            json["total_results"] == null ? null : json["total_results"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        results: json["results"] == null
            ? null
            : List<dynamic>.from(json["results"].map((x) => x)),
        hasNext: json["has_next"] == null ? null : json["has_next"],
        hasPrev: json["has_prev"] == null ? null : json["has_prev"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page": page == null ? null : page,
        "total_results": totalResults == null ? null : totalResults,
        "total_pages": totalPages == null ? null : totalPages,
        "results":
            results == null ? null : List<dynamic>.from(results.map((x) => x)),
        "has_next": hasNext == null ? null : hasNext,
        "has_prev": hasPrev == null ? null : hasPrev,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.productId,
    this.label,
    this.consumptionSize,
    this.image,
    this.price,
    this.discount,
    this.description,
    this.comment,
    this.defaultUnit,
    this.soldOut,
    this.manufacturingCompany,
    this.consumptionUnit,
    this.productCategory,
    this.disabled,
    this.offer,
    this.brand,
  });

  String productId;
  String label;
  String consumptionSize;
  List<Map<String, String>> image;
  double price;
  double discount;
  String description;
  String comment;
  DefaultUnit defaultUnit;
  bool soldOut;
  String manufacturingCompany;
  String consumptionUnit;
  ProductCategory productCategory;
  bool disabled;
  Brand offer;
  Brand brand;

  factory Datum.fromRawJson(String str) => Datum.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        productId: json["product_id"] == null ? null : json["product_id"],
        label: json["label"] == null ? null : json["label"],
        consumptionSize:
            json["consumption_size"] == null ? null : json["consumption_size"],
        image: json["image"] == null
            ? null
            : List<Map<String, String>>.from(json["image"].map((x) =>
                Map.from(x).map((k, v) => MapEntry<String, String>(k, v)))),
        price: json["price"] == null ? null : json["price"],
        discount: json["discount"] == null ? null : json["discount"],
        description: json["description"] == null ? null : json["description"],
        comment: json["comment"] == null ? null : json["comment"],
        defaultUnit: json["default_unit"] == null
            ? null
            : DefaultUnit.fromJson(json["default_unit"]),
        soldOut: json["sold_out"] == null ? null : json["sold_out"],
        manufacturingCompany: json["manufacturing_company"] == null
            ? null
            : json["manufacturing_company"],
        consumptionUnit:
            json["consumption_unit"] == null ? null : json["consumption_unit"],
        productCategory: json["product_category"] == null
            ? null
            : ProductCategory.fromJson(json["product_category"]),
        disabled: json["disabled"] == null ? null : json["disabled"],
        offer: json["offer"] == null ? null : Brand.fromJson(json["offer"]),
        brand: json["brand"] == null ? null : Brand.fromJson(json["brand"]),
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId == null ? null : productId,
        "label": label == null ? null : label,
        "consumption_size": consumptionSize == null ? null : consumptionSize,
        "image": image == null
            ? null
            : List<dynamic>.from(image.map((x) =>
                Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))),
        "price": price == null ? null : price,
        "discount": discount == null ? null : discount,
        "description": description == null ? null : description,
        "comment": comment == null ? null : comment,
        "default_unit": defaultUnit == null ? null : defaultUnit.toJson(),
        "sold_out": soldOut == null ? null : soldOut,
        "manufacturing_company":
            manufacturingCompany == null ? null : manufacturingCompany,
        "consumption_unit": consumptionUnit == null ? null : consumptionUnit,
        "product_category":
            productCategory == null ? null : productCategory.toJson(),
        "disabled": disabled == null ? null : disabled,
        "offer": offer == null ? null : offer.toJson(),
        "brand": brand == null ? null : brand.toJson(),
      };
}

class Brand {
  Brand();

  factory Brand.fromRawJson(String str) => Brand.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Brand.fromJson(Map<String, dynamic> json) => Brand();

  Map<String, dynamic> toJson() => {};
}

class DefaultUnit {
  DefaultUnit({
    this.id,
    this.name,
    this.defaultUnitDefault,
  });

  int id;
  String name;
  bool defaultUnitDefault;

  factory DefaultUnit.fromRawJson(String str) =>
      DefaultUnit.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DefaultUnit.fromJson(Map<String, dynamic> json) => DefaultUnit(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        defaultUnitDefault: json["default"] == null ? null : json["default"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "default": defaultUnitDefault == null ? null : defaultUnitDefault,
      };
}

class ProductCategory {
  ProductCategory({
    this.id,
  });

  String id;

  factory ProductCategory.fromRawJson(String str) =>
      ProductCategory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductCategory.fromJson(Map<String, dynamic> json) =>
      ProductCategory(
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
      };
}
