import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intern_project/models/cart_model.dart';
import 'package:intern_project/services/get_id.dart';

class CartApiProvider {
  final int successCode = 204;
  var storage = FlutterSecureStorage();
  String getIdUrl = "https://devapi.jvtests.com/v1/user-profile?role=PATIENT";

  Future<String> addToCart(String pid, String quantity) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'access-token': await storage.read(key: "access-token"),
      'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
    };

    var encoded = [
      {"product_id": pid, "quantity": "1"}
    ];

    final response = await getUid().then((id) async {
      String baseUrl =
          "https://devapi.jvtests.com/v1/patients/$id/carts/products";
      return await http.post(baseUrl,
          body: jsonEncode(encoded), headers: requestHeaders);
    });
    if (response.statusCode == 204) {
      return "item saved to cart";
    } else {
      throw Exception('failed to add to Cart');
    }
  }

  List<CartProduct> parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    if (response.statusCode == 200) {
      List<CartProduct> converted = [];
      for (var i = 0; i < responseString.length; i++) {
        converted.add(CartProduct.fromJson(responseString[i]));
      }
      print(converted);
      return converted;
    } else {
      throw Exception('failed to load data');
    }
  }

  Future<List<CartProduct>> loadCart() async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'access-token': await storage.read(key: "access-token"),
      'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
    };

    final response = await getUid().then((id) async {
      print("id is here >> $id");
      String baseUrl =
          "https://devapi.jvtests.com/v1/patients/$id/carts/products";
      return await http.get(baseUrl, headers: requestHeaders);
    });
    return parseResponse(response);
  }
}
