import 'package:intern_project/models/banner_model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class BannerApiProvider {
  String baseUrl = "https://devapi.jvtests.com/v1/featured-banners";
  final successCode = 200;

  Future<List<Banners>> fetchLatestBanner() async {
    final response = await http.get(baseUrl);
    return parseResponse(response);
  }

  List<Banners> parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    List<Banners> converted = [];
    if (response.statusCode == successCode) {
      for (var i = 0; i < responseString.length; i++) {
        converted.add(Banners.fromJson(responseString[i]));
      }
      return converted;
    } else {
      throw Exception('failed to load data');
    }
  }
}
