import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intern_project/models/brands_model.dart';

class FeaturedBrandsProvider {
  String baseUrl =
      "https://devapi.jvtests.com/v1/products/brands?page=1&item_per_page=20&featured=true";
  final successCode = 200;

  Future<List<Datum>> fetchLatestBanner() async {
    final response = await http.get(baseUrl);
    return parseResponse(response);
  }

  List<Datum> parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    List<BrandsBanner> converted = [];
    if (response.statusCode == successCode) {
      return BrandsBanner.fromJson(responseString).data;
    } else {
      throw Exception('failed to load data');
    }
  }
}
