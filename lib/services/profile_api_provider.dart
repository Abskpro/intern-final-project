import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intern_project/models/profile.model.dart';

class ProfileApiProvider {
  String getUrl = "https://devapi.jvtests.com/v1/user-profile?role=PATIENT";
  final successCode = 200;
  var storage = FlutterSecureStorage();

  Future<UserProfile> updateUser(String firstName, String lastName,
      String bloodType, String gender) async {
    var id = await storage.read(key: 'id');
    String baseUrl = "https://devapi.jvtests.com/v1/patients/${id}";
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'access-token': await storage.read(key: "access-token"),
      'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
    };

    var encoded = {
      "first_name": firstName,
      "last_name": lastName,
      "blood_group": bloodType,
      "gender": gender
    };

    print(encoded);
    final response = await http.put(baseUrl,
        body: jsonEncode(encoded), headers: requestHeaders);
    print(response.body);
    return parseResponse(response);
  }

  Future<UserProfile> fetchUserInfo() async {
    print("fetch user info being called here");
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
    };

    final response = await http.get(getUrl, headers: requestHeaders);
    return parseResponse(response);
  }

  UserProfile parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    print("/////// $responseString");
    if (response.statusCode == successCode) {
      var data = UserProfile.fromJson(responseString);
      void storeToken() async {
        await storage.write(key: "id", value: data.id);
      }

      storeToken();
      return UserProfile.fromJson(responseString);
    }
  }
}
