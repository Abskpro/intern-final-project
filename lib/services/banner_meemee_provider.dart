import 'package:intern_project/models/banner_meemeee_model.dart';
import 'package:intern_project/models/banner_model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class BannerMeemeeProvider {
  String baseUrl = "https://devapi.jvtests.com/v1/product-brands/banners";
  final successCode = 200;

  Future<List<Data>> fetchMeemee() async {
    final response = await http.get(baseUrl);
    return parseResponse(response);
  }

  List<Data> parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    List<Banners> converted = [];
    // print("fasdfasd ${responseString.runtimeType}");
    if (response.statusCode == successCode) {
      // print("??????${BannerMeemee.fromJson(responseString).data}");
      return BannerMeemee.fromJson(responseString).data;
    } else {
      throw Exception('failed to load data');
    }
  }
}
