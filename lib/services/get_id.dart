import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intern_project/models/profile.model.dart';

String getIdUrl = "https://devapi.jvtests.com/v1/user-profile?role=PATIENT";
var storage = FlutterSecureStorage();

Future<String> getUid() async {
  Map<String, String> requestHeaders = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'access-token': await storage.read(key: "access-token"),
    'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
  };

  return await http.get(getIdUrl, headers: requestHeaders).then((data) {
    var parsed = UserProfile.fromJson(jsonDecode(data.body));
    return parsed.id;
  });
}
