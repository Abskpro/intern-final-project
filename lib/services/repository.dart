import 'package:intern_project/models/banner_meemeee_model.dart';
import 'package:intern_project/models/banner_model.dart';
import 'package:intern_project/models/brands_model.dart';
import 'package:intern_project/models/cart_model.dart';
import 'package:intern_project/models/login_model.dart';
import 'package:intern_project/models/product_model.dart' as prod;
import 'package:intern_project/models/profile.model.dart';
import 'package:intern_project/models/register_model.dart';
import 'package:intern_project/models/wish_list_model.dart' as wish;
import 'package:intern_project/services/banner_api_provider.dart';
import 'package:intern_project/services/banner_meemee_provider.dart';
import 'package:intern_project/services/brand_banner_provider.dart';
import 'package:intern_project/services/cart_api_provider.dart';
import 'package:intern_project/services/login_api-provider.dart';
import 'package:intern_project/services/otp_api_provider.dart';
import 'package:intern_project/services/product_api_provider.dart';
import 'package:intern_project/services/register_api_provider.dart';
import 'package:intern_project/services/profile_api_provider.dart';
import 'package:intern_project/services/wish_list_provider.dart';

class BannerRepository {
  BannerApiProvider _bannerApiProvider = BannerApiProvider();

  Future<List<Banners>> fetchBanners() =>
      _bannerApiProvider.fetchLatestBanner();
}

class BrandRepository {
  FeaturedBrandsProvider _featuredBrandsProvider = FeaturedBrandsProvider();

  Future<List<Datum>> fetchBrands() =>
      _featuredBrandsProvider.fetchLatestBanner();
}

class LoginRepository {
  LoginApiProvider _loginApiProvider = LoginApiProvider();

  Future<Login> fetchUser(String number, String password) =>
      _loginApiProvider.fetchUserInfo(number, password);
}

class RegisterRepository {
  RegisterProvider _registerProvider = RegisterProvider();

  Future<Register> signUpUser(
          String number, String date, String password, String role) =>
      _registerProvider.registerUser(number, date, password, role);
}

class BannerMeemeeRepository {
  BannerMeemeeProvider _bannerMeemee = BannerMeemeeProvider();

  Future<List<Data>> fetchMeemee() => _bannerMeemee.fetchMeemee();
}

class ProductRepository {
  ProductApiProvider _productApiProvider = ProductApiProvider();

  Future<List<prod.Datum>> fetchProducts(id) =>
      _productApiProvider.fetchProducts(id);
}

class OtpRepository {
  OtpApiProvider _otpApiProvider = OtpApiProvider();

  Future<String> submitOtp(String number) => _otpApiProvider.verifyOtp(number);
}

class ProfileRepository {
  ProfileApiProvider _profileApiProvider = ProfileApiProvider();

  Future<UserProfile> updatePofile(
          String firstName, String lastName, String bloodType, String gender) =>
      _profileApiProvider.updateUser(firstName, lastName, bloodType, gender);

  Future<UserProfile> fetchUserInfo() => _profileApiProvider.fetchUserInfo();
}

class CartRepository {
  CartApiProvider _cartApiProvider = CartApiProvider();

  Future<String> updateCart(String pid, String quantity) =>
      _cartApiProvider.addToCart(pid, quantity);

  Future<List<CartProduct>> loadCart() => _cartApiProvider.loadCart();
}

class WishListRepository {
  WishListApiProvider _wishListApiProvider = WishListApiProvider();

  Future<String> updateWishList(String pid) =>
      _wishListApiProvider.addToWishList(pid);

  Future<String> deleteItem(String pid) =>
      _wishListApiProvider.deleteFromoWishList(pid);

  Future<List<wish.Datum>> loadWishList() =>
      _wishListApiProvider.loadWishList();
}
