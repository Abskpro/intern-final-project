import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intern_project/models/wish_list_model.dart';
import 'package:intern_project/services/get_id.dart';

class WishListApiProvider {
  final successCode = 200;
  var storage = FlutterSecureStorage();

  Future<String> addToWishList(String pid) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'access-token': await storage.read(key: "access-token"),
      'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
    };

    var encoded = [
      {"product_id": pid}
    ];

    final response = await getUid().then((id) async {
      String baseUrl =
          "https://devapi2.jvtests.com/v1/patients/$id/wish-lists/products";
      return await http.post(baseUrl,
          body: jsonEncode(encoded), headers: requestHeaders);
    });

    print(response.statusCode);
    if (response.statusCode == 204) {
      return "item saved to wishlist";
    } else {
      throw Exception('failed to save to wishlist');
    }
  }

  List<Datum> parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    if (response.statusCode == successCode) {
      print("llllllll >>>. ${WishList.fromJson(responseString).data[0].image}");
      return WishList.fromJson(responseString).data;
    } else {
      throw Exception('failed to load wishlist');
    }
  }

  Future<List<Datum>> loadWishList() async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'access-token': await storage.read(key: "access-token"),
      'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
    };

    final response = await getUid().then((id) async {
      print("id is here >> $id");
      String baseUrl =
          "https://devapi2.jvtests.com/v1/patients/$id/wish-lists/products";
      return await http.get(baseUrl, headers: requestHeaders);
    });
    print(response.body);
    return parseResponse(response);
  }

  Future<String> deleteFromoWishList(String pid) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'access-token': await storage.read(key: "access-token"),
      'Authorization': 'Bearer ${await storage.read(key: "authorization")}'
    };

    final response = await getUid().then((id) async {
      String baseUrl =
          "https://devapi.jvtests.com/v1/patients/$id/wish-lists/products/$pid";
      return await http.delete(baseUrl, headers: requestHeaders);
    });

    print(response.statusCode);
    if (response.statusCode == 204) {
      print("deleted");
      return "item deleted from wishlist";
    } else {
      throw Exception('failed to delete from wishlist');
    }
  }
}
