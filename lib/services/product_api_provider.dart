import 'package:intern_project/models/product_model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ProductApiProvider {
  String baseUrl = "https://devapi.jvtests.com/v1/products";
  final successCode = 200;

  Future<List<Datum>> fetchProducts(int id) async {
    // var url = "$baseUrl?category_id=104&%20page=1&item_per_page=20";
    var url =
        'https://devapi2.jvtests.com/v1/products?category_id=$id&%20page=1&item_per_page=20';
    final response = await http.get(url);
    return parseResponse(response);
  }

  List<Datum> parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    if (response.statusCode == successCode) {
      print("successful");
      print(
          "ooooooo>>>> ${ProductMeemee.fromJson(responseString).data[0].image[0]}");
      print(
          "ooooooo>>>> ${ProductMeemee.fromJson(responseString).data[0].image[0]["128"]}");
      return ProductMeemee.fromJson(responseString).data;
    } else {
      throw Exception('failed to load data');
    }
  }
}
