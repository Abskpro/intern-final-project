import 'dart:convert';
import 'package:http/http.dart' as http;

class OtpApiProvider {
  String baseUrl = "https://devapi.jvtests.com/v1/auth/confirm-otp/123456";
  final successCode = 200;

  Future<String> verifyOtp(String number) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    var encoded = {
      'username': number,
    };
    final response = await http.post(baseUrl,
        body: jsonEncode(encoded), headers: requestHeaders);
    return parseResponse(response);
  }

  String parseResponse(http.Response response) {
    if (response.statusCode == successCode) {
      return "Verified";
    } else {
      throw Exception('failed to load data');
    }
  }
}
