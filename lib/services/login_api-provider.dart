import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intern_project/models/login_model.dart';

class LoginApiProvider {
  final baseUrl = "https://devapi.jvtests.com/v1/auth/login";
  final successCode = 200;
  final serverError = 500;
  var storage = FlutterSecureStorage();

  Future<Login> fetchUserInfo(String number, String password) async {
    print(number.runtimeType);
    print(password.runtimeType);
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    var encoded = {'username': number, 'password': password};
    print(encoded);

    var response = await http.post(
      baseUrl,
      body: jsonEncode(encoded),
      headers: {'Content-Type': 'application/json'},
    );
    parseResponse(response);
  }

  Login parseResponse(response) {
    final responseString = jsonDecode(response.body);
    print("headers ${response.headers["authorization"]}");
    // print("fasdfasd ${responseString}");
    if (response.statusCode == successCode) {
      void storeToken() async {
        await storage.write(
            key: "access-token", value: response.headers["access-token"]);
        await storage.write(
            key: "authorization", value: response.headers["authorization"]);
        print("tokenset");
      }

      storeToken();
      print(Login.fromJson(responseString));
      return Login.fromJson(responseString);
    } else if (response.statusCode == serverError) {
      throw Exception('Server Error');
    } else {
      throw Exception('Number or Password is Incorrect');
    }
  }
}
