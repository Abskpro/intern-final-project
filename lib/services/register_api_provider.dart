import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intern_project/models/register_model.dart';

class RegisterProvider {
  String baseUrl = "https://devapi.jvtests.com/v1/auth/signup";
  final successCode = 200;
  final serverError = 500;

  Future<Register> registerUser(
      String number, String date, String password, String role) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    var encoded = {
      'username': number,
      'password': password,
      "date_of_birth": date,
      "referral_code": "",
      "roles": role
    };

    print(encoded);
    final response = await http.post(baseUrl,
        body: jsonEncode(encoded), headers: requestHeaders);
    print(response.body);
    return parseResponse(response);
  }

  Register parseResponse(http.Response response) {
    final responseString = jsonDecode(response.body);
    if (response.statusCode == successCode) {
      return Register.fromJson(responseString);
    } else if (response.statusCode == serverError) {
      throw Exception("Server error");
    } else {
      throw Exception("User already exists");
    }
  }
}
