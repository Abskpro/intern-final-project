import 'package:flutter/material.dart';
import 'package:intern_project/screens/homeScreen/components/body.dart';
import 'package:intern_project/widgets/appbar/drawer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            drawer: AppDrawer(),
            body: Body(),
            bottomNavigationBar: _bottomBar(context)));
  }

  Widget _bottomBar(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Colors.white,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: new TextStyle(color: Colors.black))),
        child: BottomNavigationBar(
          currentIndex: _currentIndex,
          unselectedFontSize: 12,
          selectedFontSize: 12,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.supervised_user_circle),
              label: "Appointment",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.medical_services),
              label: "Shop",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Prescription",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat),
              label: "Consultation",
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ));
  }
}
