import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intern_project/bloc/banner/banner_bloc.dart';
import 'package:intern_project/bloc/banner/banner_event.dart';
import 'package:intern_project/bloc/banner/banner_state.dart';
import 'package:intern_project/bloc/bannerMeemee/meemee_bloc.dart';
import 'package:intern_project/bloc/bannerMeemee/meemee_event.dart';
import 'package:intern_project/bloc/bannerMeemee/meemee_state.dart';
import 'package:intern_project/bloc/brands/brands_bloc.dart';
import 'package:intern_project/bloc/brands/brands_event.dart';
import 'package:intern_project/bloc/brands/brands_state.dart';
import 'package:intern_project/bloc/cart/cart_bloc.dart';
import 'package:intern_project/bloc/product/product_bloc.dart';
import 'package:intern_project/bloc/product/product_event.dart';
import 'package:intern_project/bloc/product/product_state.dart';
import 'package:intern_project/bloc/wishlist/wish_list_bloc.dart';
import 'package:intern_project/screens/homeScreen/components/background.dart';
import 'package:intern_project/screens/homeScreen/homeDisplayComponent/banner.dart';
import 'package:intern_project/screens/homeScreen/homeDisplayComponent/brands.dart';
import 'package:intern_project/screens/homeScreen/homeDisplayComponent/meeMeeBanner.dart';
import 'package:intern_project/screens/homeScreen/homeDisplayComponent/products.dart';
import 'package:intern_project/services/repository.dart';
import 'package:intern_project/widgets/buttons/form_button.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final storage = new FlutterSecureStorage();
  BannerBloc _bannerBloc;
  FeaturedBrandBloc _featuredBrandBloc;
  MeemeeBloc _meemeeBloc;
  ProductBloc _productBloc;
  BabyProductBloc _babyProductBloc;
  MedicalProductBloc _medicalProductBloc;
  CartBloc _cartBloc;
  WishListBloc _wishListBloc;

  @override
  void initState() {
    super.initState();
    _bannerBloc =
        BannerBloc(bannerRepository: context.read<BannerRepository>());
    _bannerBloc.add(BannerLoadEvent());

    _featuredBrandBloc =
        FeaturedBrandBloc(brandRepository: context.read<BrandRepository>());
    _featuredBrandBloc.add(FeaturedBrandLoad());

    _meemeeBloc = MeemeeBloc(
        bannerMeemeeRepository: context.read<BannerMeemeeRepository>());
    _meemeeBloc.add(MeemeeLoadEvent());

    _productBloc =
        ProductBloc(productRepository: context.read<ProductRepository>());

    _babyProductBloc =
        BabyProductBloc(productRepository: context.read<ProductRepository>());

    _medicalProductBloc = MedicalProductBloc(
        productRepository: context.read<ProductRepository>());

    _productBloc.add(ProductLoadEvent(id: 104, product: "skin"));
    _babyProductBloc.add(ProductLoadEvent(id: 65, product: "baby"));
    _medicalProductBloc.add(ProductLoadEvent(id: 105, product: "medical"));

    _cartBloc = CartBloc(cartRepository: context.read<CartRepository>());

    _wishListBloc =
        WishListBloc(wishListRepository: context.read<WishListRepository>());

    var token = storage.read(key: "access-token");
    print("tokenn ${token}");
  }

  @override
  void dispose() {
    super.dispose();
    _bannerBloc.close();
    _featuredBrandBloc.close();
    _meemeeBloc.close();
    _productBloc.close();
    _cartBloc.close();
    _wishListBloc.close();
  }

  void reFetchTopBanner() {
    _bannerBloc.add(BannerLoadEvent());
  }

  void reFetchFeatured() {
    _featuredBrandBloc.add(FeaturedBrandLoad());
  }

  void reFetchMeemee() {
    _meemeeBloc.add(MeemeeLoadEvent());
  }

  void reFetchProduct() {
    _productBloc.add(ProductLoadEvent(id: 104));
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (BuildContext context) => _bannerBloc),
        BlocProvider(create: (BuildContext context) => _featuredBrandBloc),
        BlocProvider(create: (BuildContext context) => _meemeeBloc),
        BlocProvider(create: (BuildContext context) => _productBloc),
        BlocProvider(create: (BuildContext context) => _babyProductBloc),
        BlocProvider(create: (BuildContext context) => _medicalProductBloc),
      ],
      child: Background(
        child: Column(
          children: [
            BlocConsumer<BannerBloc, BannerPageState>(
              listener: (context, state) {
                if (state is BannerLoadingSuccessState) {
                  // print("printed here >>>> ${state.banner}");
                }
              },
              builder: (context, state) {
                if (state is BannerLoadingSuccessState) {
                  return BannerSlideShow(
                    items: state.banner,
                  );
                } else if (state is BannerLoadingFailedState) {
                  return Container(
                    height: size.height * 0.25,
                    child: Column(
                      children: [
                        Text("Error Loading Data"),
                        FormBtn(
                            text: "Retry",
                            press: reFetchTopBanner,
                            screen: "banner",
                            width: 0.15,
                            color: Colors.redAccent)
                      ],
                    ),
                  );
                } else {
                  return Container(
                    height: size.height * 0.15,
                    child: Center(child: CircularProgressIndicator()),
                  );
                }
              },
            ),
            BlocConsumer<FeaturedBrandBloc, FeaturedBrandsState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is FeaturedBrandsStateSuccess) {
                  return Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            bottomLeft: Radius.circular(15)),
                        color: Colors.white),
                    margin: EdgeInsets.only(left: 15),
                    height: 120,
                    child: Column(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Brands"),
                              Text("View all",
                                  style: TextStyle(color: Colors.blue))
                            ],
                          ),
                        ),
                        Container(
                          height: 90,
                          child: Brands(
                            brands: state.banner,
                          ),
                        ),
                      ],
                    ),
                  );
                } else if (state is FeaturedBrandsLoadFailed) {
                  return Container(
                    height: size.height * 0.25,
                    child: Column(
                      children: [
                        Text("Error Loading Data"),
                        FormBtn(
                          text: "Retry",
                          press: reFetchFeatured,
                          screen: "featured",
                          color: Colors.redAccent,
                          width: 0.15,
                        )
                      ],
                    ),
                  );
                } else {
                  return Container(
                    height: size.height * 0.25,
                    child: Center(child: CircularProgressIndicator()),
                  );
                }
              },
            ),
            SizedBox(
              height: 10,
            ),
            BlocConsumer<MeemeeBloc, MeemeePageState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is MeemeeSuccessState) {
                  return MeeMeeBanner(
                    banner: state.banner,
                  );
                } else if (state is MeemeeLoadFailedState) {
                  return Container(
                    height: size.height * 0.25,
                    child: Column(
                      children: [
                        Text("Error Loading Data"),
                        FormBtn(
                            text: "Retry",
                            press: reFetchMeemee,
                            screen: "meemee",
                            width: 0.15,
                            color: Colors.redAccent)
                      ],
                    ),
                  );
                } else {
                  return Container(
                    height: size.height * 0.3,
                    child: Center(child: CircularProgressIndicator()),
                  );
                }
              },
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: BlocConsumer<ProductBloc, ProductPageState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is SkinProductSuccessState) {
                    return ProductsCard(
                      product: state.banner,
                      bloc1: _cartBloc,
                      bloc2: _wishListBloc,
                      category: "Skin Care",
                    );
                  } else {
                    return Container(
                      height: size.height * 0.3,
                      child: Center(child: CircularProgressIndicator()),
                    );
                  }
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: BlocConsumer<BabyProductBloc, BabyProductPageState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is BabyProductSuccessState) {
                    return ProductsCard(
                      product: state.banner,
                      bloc1: _cartBloc,
                      bloc2: _wishListBloc,
                      category: "Baby",
                    );
                  } else {
                    return Container(
                      height: size.height * 0.3,
                      child: Center(child: CircularProgressIndicator()),
                    );
                  }
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: BlocConsumer<MedicalProductBloc, MedicalProductPageState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is MedicalDeviceProductSuccessState) {
                    return ProductsCard(
                      product: state.banner,
                      bloc1: _cartBloc,
                      bloc2: _wishListBloc,
                      category: "Medical Devices",
                    );
                  } else {
                    return Container(
                      height: size.height * 0.3,
                      child: Center(child: CircularProgressIndicator()),
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
