import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/models/banner_model.dart';

class BannerSlideShow extends StatelessWidget {
  final List<Banners> items;
  BannerSlideShow({this.items});
  @override
  Widget build(BuildContext context) {
    print(items[0].image);
    return Column(
      children: [
        SizedBox(
          height: 5,
        ),
        CarouselSlider(
            options: CarouselOptions(
                height: MediaQuery.of(context).size.width * 0.36,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 10)),
            items: items.map((card) {
              return Builder(builder: (BuildContext context) {
                return ClipRRect(
                  borderRadius: BorderRadius.circular(42.0),
                  child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      height: 140,
                      child: Image.network(
                        card.image,
                        fit: BoxFit.contain,
                      )),
                );
              });
            }).toList()),
      ],
    );
  }
}
