import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intern_project/bloc/cart/cart_bloc.dart';
import 'package:intern_project/bloc/cart/cart_event.dart';
import 'package:intern_project/bloc/cart/cart_state.dart';
import 'package:intern_project/bloc/wishlist/wish_list_bloc.dart';
import 'package:intern_project/bloc/wishlist/wish_list_event.dart';
import 'package:intern_project/bloc/wishlist/wish_list_state.dart';
import 'package:intern_project/models/product_model.dart';
import 'package:intern_project/screens/productDetailScreen/product_detail_screen.dart';
import 'package:intern_project/widgets/buttons/product_button.dart';

class ProductsCard extends StatefulWidget {
  final List<Datum> product;
  final String category;
  final bloc1;
  final bloc2;
  ProductsCard({this.product, this.category, this.bloc1, this.bloc2});

  @override
  _ProductsCardState createState() => _ProductsCardState();
}

class _ProductsCardState extends State<ProductsCard> {
  void wishlist(String id) {
    print(id);
    widget.bloc2.add(WishListButtonPressed(pid: id));
  }

  void addToCart(String id) {
    widget.bloc1.add(CartButtonPressedEvent(pid: id));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MultiBlocProvider(
          providers: [
            BlocProvider<CartBloc>(
              create: (BuildContext context) => widget.bloc1,
            ),
            BlocProvider<WishListBloc>(
              create: (BuildContext context) => widget.bloc2,
            ),
          ],
          child: MultiBlocListener(
            listeners: [
              BlocListener<CartBloc, CartState>(
                listener: (BuildContext context, state) {
                  if (state is CartStateFailed) {
                    Fluttertoast.showToast(
                      msg: state.message,
                      toastLength: Toast.LENGTH_SHORT,
                      timeInSecForIosWeb: 2,
                      backgroundColor: Colors.tealAccent,
                      textColor: Colors.white,
                      fontSize: 18.0,
                    );
                  }
                  if (state is CartStateSuccess) {
                    Fluttertoast.showToast(
                      msg: state.message,
                      toastLength: Toast.LENGTH_SHORT,
                      timeInSecForIosWeb: 2,
                      backgroundColor: Colors.tealAccent,
                      textColor: Colors.white,
                      fontSize: 18.0,
                    );
                  }
                },
              ),
              BlocListener<WishListBloc, WishListState>(
                listener: (BuildContext context, state) {
                  if (state is WishListSavedFailed) {
                    Fluttertoast.showToast(
                      msg: state.message,
                      toastLength: Toast.LENGTH_SHORT,
                      timeInSecForIosWeb: 2,
                      backgroundColor: Colors.tealAccent,
                      textColor: Colors.white,
                      fontSize: 18.0,
                    );
                  }
                  if (state is WishListStateSuccess) {
                    Fluttertoast.showToast(
                      msg: state.message,
                      toastLength: Toast.LENGTH_SHORT,
                      timeInSecForIosWeb: 2,
                      backgroundColor: Colors.tealAccent,
                      textColor: Colors.white,
                      fontSize: 18.0,
                    );
                  }
                },
              )
            ],
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Explore",
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w300)),
                      Text("View all", style: TextStyle(color: Colors.blue)),
                    ],
                  ),
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Text(widget.category)),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 250,
          child: new ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: widget.product.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return ProductDetail(
                        product: widget.product[index],
                      );
                    }));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.white,
                    ),
                    width: 170,
                    height: 100,
                    margin: EdgeInsets.fromLTRB(10, 0, 0, 10),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Stack(children: [
                        Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                      width: 1.0, color: Colors.grey),
                                ),
                                color: Colors.white,
                              ),
                              height: 100,
                              child: Image.network(
                                widget.product[index].image[0]["128"],
                                fit: BoxFit.contain,
                              ),
                            ),
                            Divider(),
                            Column(
                              children: [
                                Text(widget.product[index].label,
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w600)),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                      widget
                                          .product[index].manufacturingCompany,
                                      style: TextStyle(fontSize: 10)),
                                ),
                                SizedBox(
                                  height: 9,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "NPR. ${widget.product[index].price.toString()}",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w500,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                    Text(
                                      "NPR. ${(widget.product[index].price - ((widget.product[index].price * widget.product[index].discount) / 100)).toInt().toString()}",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ProductBtn(
                                      text: "WishList",
                                      color: Colors.redAccent,
                                      width: 0.18,
                                      id: widget.product[index].productId,
                                      height: 0.04,
                                      fontSize: 8,
                                      press: wishlist,
                                    ),
                                    ProductBtn(
                                      text: "Add to cart",
                                      color: Colors.greenAccent[400],
                                      width: 0.18,
                                      id: widget.product[index].productId,
                                      height: 0.04,
                                      fontSize: 8,
                                      press: addToCart,
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                        Positioned(
                          top: 10,
                          child: Container(
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                color: Colors.blue,
                              ),
                              width: 50,
                              child:
                                  Text("-${widget.product[index].discount}%")),
                        )
                      ]),
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }
}
