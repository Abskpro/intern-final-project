import 'package:flutter/material.dart';
import 'package:intern_project/models/brands_model.dart';

class Brands extends StatelessWidget {
  final List<Datum> brands;
  Brands({this.brands});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: brands.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              width: 110,
              margin: EdgeInsets.fromLTRB(10, 0, 0, 10),
              child: Image.network(
                brands[index].images["128"],
                fit: BoxFit.contain,
              ),
            );
          }),
    );
  }
}
