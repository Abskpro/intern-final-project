import 'package:flutter/material.dart';
import 'package:intern_project/models/banner_meemeee_model.dart';

class MeeMeeBanner extends StatelessWidget {
  final List<Data> banner;
  MeeMeeBanner({this.banner});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50), color: Colors.red),
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Image.network(
        MediaQuery.of(context).size.width > 500
            ? banner[0].images.s1920
            : banner[0].images.s512,
        fit: BoxFit.contain,
      ),
    );
  }
}
