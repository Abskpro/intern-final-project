import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/bloc/updateProfile/profile_update_bloc.dart';
import 'package:intern_project/bloc/updateProfile/profile_update_event.dart';
import 'package:intern_project/bloc/updateProfile/profile_update_state.dart';
import 'package:intern_project/screens/profileScreen/components/background.dart';
import 'package:intern_project/services/repository.dart';
import 'package:intern_project/widgets/buttons/form_button.dart';
import 'package:intern_project/widgets/inputField/dropdown.dart';
import 'package:intern_project/widgets/inputField/input_field.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String firstName = "";
  String lastName = "";
  String selectedBlood = "O+";
  String selectedGender = "MALE";
  List<String> _bloodType = ["A+", "B+", "O+", "O-", "A-", "AB-"];
  List<String> _gender = ["MALE", "FEMALE"];

  ProfileBloc _profileBloc;

  void initState() {
    super.initState();
    _profileBloc =
        ProfileBloc(profileRepository: context.read<ProfileRepository>());
    _profileBloc.add(ProfileLoadEvent());
  }

  void onChanged(String value, String type) {
    if (type == "blood") {
      setState(() {
        selectedBlood = value;
      });
    } else if (type == "gender") {
      setState(() {
        selectedGender = value;
      });
    } else if (type == "firstName") {
      setState(() {
        firstName = value;
      });
    } else {
      setState(() {
        lastName = value;
      });
    }
  }

  void revert() {
    print("Reverting");
  }

  void save() {
    print("Saving");
    _profileBloc.add(UpdateButtonPressed(
        firstName: firstName,
        lastName: lastName,
        bloodGroup: selectedBlood,
        gender: selectedGender));
    print(firstName);
  }

  void _settingModalBottomSheet(context) async {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter selectGender) {
            return Container(
                height: MediaQuery.of(context).size.height / 1.5,
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Edit you Info'),
                        IconButton(
                            icon: Icon(Icons.close_rounded),
                            onPressed: () {
                              Navigator.pop(context);
                            })
                      ],
                    ),
                    Divider(
                      color: Colors.black,
                    ),
                    TextInputField(
                      hintText: "First Name",
                      onChanged: onChanged,
                      input: "firstName",
                    ),
                    TextInputField(
                      hintText: "Last Name",
                      onChanged: onChanged,
                      input: "lastName",
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        DropDown(
                            dropdownValue: selectedBlood,
                            item: _bloodType,
                            onChanged: onChanged,
                            type: "blood"),
                        DropDown(
                          dropdownValue: selectedGender,
                          item: _gender,
                          onChanged: onChanged,
                          type: "gender",
                        ),
                      ],
                    ),
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FormBtn(
                              width: 0.4,
                              press: revert,
                              text: "Revert",
                              screen: "profile",
                              color: Colors.red),
                          FormBtn(
                              width: 0.4,
                              press: save,
                              text: "Save Changes",
                              screen: "profile",
                              color: Colors.greenAccent)
                        ],
                      ),
                    )
                  ],
                ));
          });
        });
  }

  String calculateDate(DateTime bdate) {
    double currYear = DateTime.now().year +
        (DateTime.now().month / 12) +
        (DateTime.now().day / 365);
    double bYear = (bdate.year) + (bdate.month / 12) + (bdate.day / 365);

    return (currYear - bYear).toInt().toString();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileBloc>(
      create: (BuildContext context) => _profileBloc,
      child: Background(
        child: Container(
          height: 120,
          margin: EdgeInsets.only(top: 10),
          width: double.infinity,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.white),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10, left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Basic Profile",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          _settingModalBottomSheet(context);
                        })
                  ],
                ),
              ),
              BlocConsumer<ProfileBloc, ProfileState>(
                  listener: (context, state) {
                if (state is ProfileUpdateSuccesssState) {
                  print("updated");
                  Navigator.pop(context);
                  _profileBloc.add(ProfileLoadEvent());
                }
              }, builder: (context, state) {
                if (state is ProfileLoadingState) {
                  return Container(
                    child: CircularProgressIndicator(),
                  );
                } else if (state is ProfileLoadSuccessState) {
                  return Row(children: [
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: CircleAvatar(
                        radius: 32,
                        backgroundColor: Colors.blue,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${state.user.firstName} ${state.user.lastName}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text("${calculateDate(state.user.dateOfBirth)} years"),
                        Text("Blood Type ${state.user.bloodGroup}"),
                        Text("Gender ${state.user.gender}")
                      ],
                    )
                  ]);
                } else {
                  return Container();
                }
              })
            ],
          ),
        ),
      ),
    );
  }
}
