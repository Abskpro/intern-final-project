import 'package:flutter/material.dart';
import 'package:intern_project/widgets/appbar/app_bar.dart';

class Background extends StatefulWidget {
  final Widget child;
  const Background({this.child});
  @override
  _BackgroundState createState() => _BackgroundState();
}

class _BackgroundState extends State<Background> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.grey[300],
      child: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
                [Container(height: size.height * 0.88, child: widget.child)]),
          )
        ],
      ),
    );
  }
}
