import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intern_project/bloc/cart/cart_bloc.dart';
import 'package:intern_project/bloc/cart/cart_event.dart';
import 'package:intern_project/bloc/cart/cart_state.dart';
import 'package:intern_project/bloc/wishlist/wish_list_bloc.dart';
import 'package:intern_project/bloc/wishlist/wish_list_event.dart';
import 'package:intern_project/bloc/wishlist/wish_list_state.dart';
import 'package:intern_project/models/product_model.dart';
import 'package:intern_project/screens/productDetailScreen/components/background.dart';
import 'package:intern_project/services/repository.dart';
import 'package:intern_project/widgets/buttons/product_button.dart';

class Body extends StatefulWidget {
  final Datum product;
  Body({this.product});
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  WishListBloc _wishListBloc;
  CartBloc _cartBloc;

  void initState() {
    super.initState();
    _cartBloc = CartBloc(cartRepository: context.read<CartRepository>());
    _wishListBloc =
        WishListBloc(wishListRepository: context.read<WishListRepository>());
  }

  void wishlist(String id) {
    print(id);
    _wishListBloc.add(WishListButtonPressed(pid: id));
  }

  void addToCart(String id) {
    print(id);
    _cartBloc.add(CartButtonPressedEvent(pid: id));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    print(widget.product);
    return Background(
      child: Stack(
        children: [
          Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: size.height * 0.5,
                color: Colors.white,
                child: Image.network(widget.product.image[0]["512"],
                    fit: BoxFit.contain),
              )),
          MultiBlocProvider(
            providers: [
              BlocProvider<CartBloc>(
                create: (BuildContext context) => _cartBloc,
              ),
              BlocProvider<WishListBloc>(
                create: (BuildContext context) => _wishListBloc,
              ),
            ],
            child: MultiBlocListener(
              listeners: [
                BlocListener<CartBloc, CartState>(
                  listener: (BuildContext context, state) {
                    if (state is CartStateFailed) {
                      Fluttertoast.showToast(
                        msg: state.message,
                        toastLength: Toast.LENGTH_SHORT,
                        timeInSecForIosWeb: 2,
                        backgroundColor: Colors.tealAccent,
                        textColor: Colors.white,
                        fontSize: 18.0,
                      );
                    }
                    if (state is CartStateSuccess) {
                      Fluttertoast.showToast(
                        msg: state.message,
                        toastLength: Toast.LENGTH_SHORT,
                        timeInSecForIosWeb: 2,
                        backgroundColor: Colors.tealAccent,
                        textColor: Colors.white,
                        fontSize: 18.0,
                      );
                    }
                  },
                ),
                BlocListener<WishListBloc, WishListState>(
                  listener: (BuildContext context, state) {
                    if (state is WishListSavedFailed) {
                      Fluttertoast.showToast(
                        msg: state.message,
                        toastLength: Toast.LENGTH_SHORT,
                        timeInSecForIosWeb: 2,
                        backgroundColor: Colors.tealAccent,
                        textColor: Colors.white,
                        fontSize: 18.0,
                      );
                    }
                    if (state is WishListStateSuccess) {
                      Fluttertoast.showToast(
                        msg: state.message,
                        toastLength: Toast.LENGTH_SHORT,
                        timeInSecForIosWeb: 2,
                        backgroundColor: Colors.tealAccent,
                        textColor: Colors.white,
                        fontSize: 18.0,
                      );
                    }
                  },
                )
              ],
              child: DraggableScrollableSheet(
                  initialChildSize: 0.5,
                  minChildSize: 0.5,
                  maxChildSize: 0.999,
                  builder: (BuildContext context,
                      ScrollController scrollController) {
                    return SingleChildScrollView(
                      controller: scrollController,
                      child: Container(
                        color: Colors.white,
                        height: size.height,
                        child: Column(
                          children: [
                            Divider(
                              color: Colors.blueGrey,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Flex(
                                    direction: Axis.horizontal,
                                    children: [
                                      Text(widget.product.label),
                                      Spacer(),
                                      Icon(Icons.share)
                                    ],
                                  ),
                                  Text(widget.product.manufacturingCompany),
                                  Flex(
                                    direction: Axis.horizontal,
                                    children: [
                                      Text(
                                        "NPR. ${widget.product.price.toString()}",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            decoration:
                                                TextDecoration.lineThrough),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text("-${widget.product.discount}%",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                              decoration:
                                                  TextDecoration.lineThrough))
                                    ],
                                  ),
                                  Flex(
                                    direction: Axis.horizontal,
                                    children: [
                                      Text(
                                        "NPR. ${(widget.product.price - ((widget.product.price * widget.product.discount) / 100)).toInt().toString()}",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Spacer(),
                                      Flex(
                                        direction: Axis.horizontal,
                                        children: [
                                          IconButton(
                                              icon: Icon(Icons.remove_circle),
                                              onPressed: () {}),
                                          Text("1"),
                                          IconButton(
                                              icon: Icon(Icons.add_circle),
                                              onPressed: () {})
                                        ],
                                      )
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.blueGrey,
                                  ),
                                  Flex(
                                    direction: Axis.vertical,
                                    children: [
                                      Text("Product Detail"),
                                      Html(data: widget.product.description)
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ),
          Positioned(
            top: size.height * 0.82,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1.0, color: Colors.grey),
                ),
                color: Colors.white,
              ),
              child: Row(
                children: [
                  ProductBtn(
                    text: "WishList",
                    color: Colors.greenAccent[400],
                    width: 0.45,
                    id: widget.product.productId,
                    height: 0.05,
                    fontSize: 12,
                    press: wishlist,
                  ),
                  ProductBtn(
                    text: "Add to Cart",
                    color: Colors.redAccent,
                    width: 0.45,
                    id: widget.product.productId,
                    height: 0.05,
                    fontSize: 12,
                    press: addToCart,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
