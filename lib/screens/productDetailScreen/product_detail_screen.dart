import 'package:flutter/material.dart';
import 'package:intern_project/models/product_model.dart';
import 'package:intern_project/screens/productDetailScreen/components/body.dart';

class ProductDetail extends StatelessWidget {
  final Datum product;
  ProductDetail({this.product});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.teal[400],
            title: Text(
              product.label,
              style: TextStyle(fontSize: 15),
            )),
        body: Body(
          product: product,
        ),
      ),
    );
  }
}
