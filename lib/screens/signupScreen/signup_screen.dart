import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/screens/signupScreen/components/body.dart';
import 'package:intern_project/services/repository.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider<RegisterRepository>(
      create: (context) => RegisterRepository(),
      child: SafeArea(
        child: Scaffold(
          body: Body(),
        ),
      ),
    );
  }
}
