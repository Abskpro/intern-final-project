import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intern_project/bloc/register/register_bloc.dart';
import 'package:intern_project/bloc/register/register_event.dart';
import 'package:intern_project/bloc/register/register_state.dart';
import 'package:intern_project/screens/signupScreen/components/background.dart';
import 'package:intern_project/screens/verifyScreen/verify_screen.dart';
import 'package:intern_project/services/repository.dart';
import 'package:intern_project/widgets/buttons/form_button.dart';
import 'package:intern_project/widgets/inputField/number_input.dart';
import 'package:intern_project/widgets/inputField/password_input.dart';
import 'package:intern_project/widgets/inputField/text_field_container.dart';
import 'package:intern_project/widgets/redirect/signin.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  DateTime _dateTime;
  double _year = 0;

  bool _lights = true;
  bool _value = false;

  String number = "";
  String password = "";
  String confirmPassword = "";
  String _date = "Date of Birth*";

  RegisterBloc _registerBloc;

  TextEditingController numberController;
  TextEditingController passwordController;
  TextEditingController passwordConfirmController;

  void initState() {
    super.initState();
    numberController = TextEditingController();
    passwordController = TextEditingController();
    passwordConfirmController = TextEditingController();
    numberController.addListener(_updateNumber);
    passwordController.addListener(_updatePassword);
    passwordConfirmController.addListener(_updateConfirmPassword);

    _registerBloc =
        RegisterBloc(registerRepository: context.read<RegisterRepository>());
  }

  _updateNumber() {
    setState(() {
      number = numberController.text;
    });
  }

  _updatePassword() {
    setState(() {
      password = passwordController.text;
    });
  }

  _updateConfirmPassword() {
    setState(() {
      confirmPassword = passwordConfirmController.text;
    });
  }

  bool validateNumber({checkEmpty: false}) {
    return (numberController.text.isEmpty && !checkEmpty) ||
        numberController.text.length == 10;
  }

  bool validatePassword({checkEmpty: false}) {
    print('validating password');
    return (password.isEmpty && !checkEmpty) || password.length > 10;
  }

  bool validateConfirmPassword({checkEmpty: false}) {
    print('validating confirmpassword');
    return (confirmPassword.isEmpty && !checkEmpty) ||
        confirmPassword == password;
  }

  bool validateDate({checkEmpty: false}) {
    double currYear = DateTime.now().year +
        (DateTime.now().month / 12) +
        (DateTime.now().day / 365);
    if (_year != 0) {
      return (_date.isEmpty && !checkEmpty) ||
          ((currYear - _year) >= 18 ? true : false);
    } else {
      return false;
    }
  }

  bool validateCheckbox() {
    return _value;
  }

  void validate() async {
    var role = _lights ? "PATIENT" : "Doctor";
    _registerBloc.add(RegisterButtonPressed(
        number: number, date: _date, password: password, role: role));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocProvider<RegisterBloc>(
      create: (BuildContext context) => _registerBloc,
      child: Background(
        bgColor: _lights ? Colors.greenAccent : Colors.blueAccent,
        child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          child: Form(
            key: formkey,
            child: BlocConsumer<RegisterBloc, RegisterState>(
                listener: (context, state) {
              if (state is RegisterSuccessState) {
                print("printing");
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return VerifyScreen(
                    number: number,
                  );
                }));
              }
              if (state is RegisterLoadingState) {
                Scaffold.of(context).showSnackBar(SnackBar(
                    content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Registering In"),
                    CircularProgressIndicator(),
                  ],
                )));
              }
            }, builder: (context, state) {
              return Column(
                children: [
                  SizedBox(
                    height: 150,
                  ),
                  Container(
                    height: 230,
                    child: Image.asset("assets/images/form-bg.png",
                        fit: BoxFit.cover),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _toggleButton(context),
                  NumberInput(
                    controller: numberController,
                  ),
                  _altDate(context),
                  // _dateBuilder(context),
                  PasswordInput(
                    text: "Password *",
                    controller: passwordController,
                  ),
                  PasswordInput(
                    text: "Confirm Password *",
                    controller: passwordConfirmController,
                  ),
                  _referralCodeInput(context),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      _checkBox(context),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        child: Text(
                            "I agreee to Jeevee's Terms and Conditions and Privacy Policy."),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  FormBtn(
                    width: 0.55,
                    text: "Join Jeeve",
                    screen: "signup",
                    validateNumber: validateNumber,
                    validatePassword: validatePassword,
                    validateConfirmPassword: validateConfirmPassword,
                    validateCheckBox: validateCheckbox,
                    color: Colors.lightGreen,
                    validateDate: validateDate,
                    press: validate,
                  ),
                  SizedBox(height: 10),
                  SignIn(text: "Go to SignIn"),
                  SizedBox(height: 20),
                ],
              );
            }),
          ),
        ),
      ),
    );
  }

  Widget _toggleButton(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Transform.scale(
            scale: 0.8,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _lights = !_lights;
                });
                print(_lights);
              },
              child: CupertinoSwitch(
                trackColor: Colors.blueAccent,
                activeColor: Colors.greenAccent,
                value: _lights,
                onChanged: (bool value) {
                  print(value);
                  setState(() {
                    _lights = value;
                  });
                },
              ),
            ),
          ),
          Text(_lights ? 'Patient SignUp' : 'Doctor SignUp',
              style: TextStyle(
                  color: _lights ? Colors.greenAccent : Colors.blueAccent)),
        ],
      ),
    );
  }

  Widget _referralCodeInput(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        // controller: widget.controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: InputDecoration(
            hintText: "Referral Code",
            contentPadding: EdgeInsets.only(top: 5, left: 15),
            hintStyle: TextStyle(color: Colors.grey),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(29))),
      ),
    );
  }

  Widget _checkBox(BuildContext context) {
    return Center(
      child: Transform.scale(
        scale: 0.7,
        child: GestureDetector(
          onTap: () {
            setState(() {
              _value = !_value;
            });
            print(">>>> $_value");
          },
          child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _value ? Colors.greenAccent : Colors.greenAccent),
              child: _value
                  ? Icon(Icons.check, size: 30.0, color: Colors.white)
                  : Icon(Icons.circle, size: 30.0, color: Colors.white)),
        ),
      ),
    );
  }

  Widget _altDate(BuildContext context) {
    return TextFieldContainer(
      child: GestureDetector(
          child: Container(
            child: TextFormField(
              enabled: false,
              decoration: InputDecoration(
                  hintText: _date,
                  contentPadding: EdgeInsets.only(top: 5, left: 15),
                  suffixIcon: Icon(Icons.calendar_today),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(29))),
            ),
          ),
          onTap: () {
            DatePicker.showDatePicker(context,
                theme: DatePickerTheme(
                  containerHeight: 210.0,
                ),
                showTitleActions: true,
                minTime: DateTime(1960, 1, 1),
                maxTime: DateTime.now(), onConfirm: (date) {
              print('confirm $date');
              setState(() {
                _dateTime = date;
                _date = '${date.year}-${date.month}-${date.day}';
                _year = date.year + (date.month / 12) + (date.day / 365);
              });
            }, currentTime: DateTime.now(), locale: LocaleType.en);
          }),
    );
  }

//   Widget _dateBuilder(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 20.0),
//       child: Container(
//         // padding: EdgeInsets.symmetric(horizontal: 20.0),
//         height: 65,
//         width: 360,
//         child: GestureDetector(
//           onTap: () => _selectDate(),
//           child: AbsorbPointer(
//             child: TextField(
//               controller: _dateController,
//               decoration: InputDecoration(
//                   suffixIcon: InkWell(
//                       onTap: () {},
//                       child: Icon(Icons.calendar_today,
//                           color:
//                               _lights ? Colors.tealAccent[400] : Colors.blue)),
//                   contentPadding:
//                       EdgeInsets.symmetric(vertical: 10, horizontal: 20),
//                   border: OutlineInputBorder(
//                     borderRadius: BorderRadius.all(Radius.circular(80.0)),
//                     borderSide: BorderSide(color: Colors.black12),
//                     //borderSide: const BorderSide(),
//                   ),
//                   hintStyle: TextStyle(color: Colors.black38),
//                   hintText: 'Date of Birth *'),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
}

// _selectDate() async {
//   DateTime finalDate = await showModalBottomSheet<DateTime>(
//     context: context,
//     builder: (context) {
//       DateTime pickedDate;
//       return Container(
//         height: 250,
//         child: Column(
//           children: <Widget>[
//             Container(
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   CupertinoButton(
//                     child: Text('Cancel',
//                         style: TextStyle(color: Colors.black38)),
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                   ),
//                   CupertinoButton(
//                     child: Text('Done'),
//                     onPressed: () {
//                       Navigator.of(context).pop(pickedDate);
//                     },
//                   ),
//                 ],
//               ),
//             ),
//             Expanded(
//               child: Container(
//                 padding: EdgeInsets.symmetric(horizontal: 5),
//                 child: CupertinoDatePicker(
//                   mode: CupertinoDatePickerMode.date,
//                   minimumYear: 1990,
//                   use24hFormat: true,
//                   onDateTimeChanged: (DateTime date) {
//                     pickedDate = date;
//                   },
//                   maximumDate: DateTime.now().add(Duration(days: 1)),
//                   //initialDateTime cannot be greater than maximumDate
//                   initialDateTime: DateTime.now(),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       );
//     },
//   );
//   if (finalDate != null && finalDate != _dateTime) {
//     setState(() {
//       _dateTime = finalDate;
//       _dateController.text = finalDate.toString();
//     });
//   }
// }
