import 'package:flutter/material.dart';
import 'package:intern_project/widgets/appbar/form_app_bar.dart';

class Background extends StatelessWidget {
  final Widget child;
  final Color bgColor;
  const Background({this.child, this.bgColor});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Stack(
        alignment: Alignment.topLeft,
        children: [
          ClipPath(
            clipper: BackgroundClipper(),
            child: Container(
              width: size.width,
              height: size.height / 2,
              decoration: BoxDecoration(color: bgColor),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, top: 10),
            child: FormAppBar(
              bgColor: bgColor,
            ),
          ),
          Padding(padding: EdgeInsets.symmetric(horizontal: 40), child: child),
          Center(
            child: Container(
                margin: EdgeInsets.only(top: 30),
                height: 150,
                child: Image.asset("assets/images/logo.png")),
          )
        ],
      ),
    );
  }
}

class BackgroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    // print("height >>>>> ${size.height}");
    // print("weight >>>>> ${size.width}");
    path.lineTo(0, size.height * 0.48);
    var controlPoint = Offset(250, size.height);
    var endPoint = Offset(size.width / 3.5, size.height);
    path.quadraticBezierTo(
        controlPoint.dx, controlPoint.dy, endPoint.dx * 4, endPoint.dy * 0.55);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);

    // print(size.height);
    // path.lineTo(0, size.height * 0.43);
    // path.quadraticBezierTo(200, size.width * 1.3, size.height * 2, 0);
    // path.lineTo(size.width, size.height);
    // //nochange
    // path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
