import 'package:flutter/material.dart';
import 'package:intern_project/screens/passwordResetScreen/components/background.dart';
import 'package:intern_project/widgets/buttons/form_button.dart';
import 'package:intern_project/widgets/inputField/number_input.dart';
import 'package:intern_project/widgets/redirect/signin.dart';
import 'package:intern_project/widgets/redirect/signup.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String number = "";
  TextEditingController numberController;

  void initState() {
    super.initState();
    numberController = TextEditingController();
    numberController.addListener(_updateNumber);
  }

  _updateNumber() {
    print('validating number');
    setState(() {
      number = numberController.text;
    });
  }

  bool validateNumber({checkEmpty: false}) {
    return (numberController.text.isEmpty && !checkEmpty) ||
        numberController.text.length == 10;
  }

  void validate() async {
    print("validating");
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(
                height: 150,
              ),
              Container(
                height: 300,
                child:
                    Image.asset("assets/images/forgot.png", fit: BoxFit.cover),
              ),
              Center(
                child: Text("Forgot Your Password?",
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              ),
              SizedBox(
                height: 20,
              ),
              NumberInput(
                controller: numberController,
              ),
              FormBtn(
                width: 0.42,
                text: "Proceed",
                screen: "reset",
                validateNumber: validateNumber,
                press: validate,
                color: Colors.lightGreen,
              ),
              SizedBox(
                height: 10,
              ),
              SignIn(text: "SignIn"),
              SizedBox(
                height: 20,
              ),
              SignUp(text: "Sign Up"),
            ],
          )),
    );
  }
}
