import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/screens/cartScreen/components/body.dart';
import 'package:intern_project/services/repository.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider<CartRepository>(
      create: (context) => CartRepository(),
      child: SafeArea(
        child: Scaffold(
          body: Body(),
        ),
      ),
    );
  }
}
