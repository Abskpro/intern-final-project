import 'package:flutter/material.dart';
import 'package:intern_project/bloc/cart/cart_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/bloc/cart/cart_event.dart';
import 'package:intern_project/bloc/cart/cart_state.dart';
import 'package:intern_project/screens/cartScreen/components/background.dart';
import 'package:intern_project/services/repository.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  CartBloc _cartBloc;
  double Total = 0;

  void initState() {
    super.initState();
    _cartBloc = CartBloc(cartRepository: context.read<CartRepository>());
    _cartBloc.add(CartLoadEvent());
  }

  void sum(value) {
    print(value.runtimeType);
    // setState(() {
    //   Total = Total + value.toDouble();
    // });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: BlocProvider<CartBloc>(
          create: (BuildContext context) => _cartBloc,
          child: BlocConsumer<CartBloc, CartState>(listener: (context, state) {
            if (state is CartLoadSuccess) {
              for (var item in state.banner) {
                print("fasdf");
                setState(() {
                  Total = Total +
                      (item.price - ((item.price * item.discount) / 100));
                });
              }
            }
          }, builder: (context, state) {
            if (state is CartLoadSuccess) {
              return Stack(children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  height: MediaQuery.of(context).size.height * 0.9,
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: state.banner.length,
                    itemBuilder: (context, index) {
                      sum((state.banner[index].price -
                          ((state.banner[index].price *
                                  state.banner[index].discount) /
                              100)));
                      return Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            border: Border.all(color: Colors.grey),
                            color: Colors.white),
                        child: Row(children: [
                          Container(
                            height: 100,
                            width: 80,
                            child: Image.network(
                              state.banner[index].image[0].s128,
                              fit: BoxFit.contain,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 200,
                                child: Text(
                                  state.banner[index].label,
                                ),
                              ),
                              Row(
                                children: [
                                  IconButton(
                                      icon: Icon(Icons.remove_circle),
                                      onPressed: () {}),
                                  Text("1"),
                                  IconButton(
                                      icon: Icon(Icons.add_circle),
                                      onPressed: () {})
                                ],
                              )
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              IconButton(
                                  icon: Icon(Icons.close_sharp),
                                  onPressed: null),
                              SizedBox(
                                height: 15,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Flex(direction: Axis.horizontal, children: [
                                    Text(
                                      "NPR. ${state.banner[index].price.toString()}",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w500,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text("-${state.banner[index].discount}%",
                                        style: TextStyle(
                                          fontSize: 10,
                                        ))
                                  ]),
                                  Text(
                                    "NPR. ${(state.banner[index].price - ((state.banner[index].price * state.banner[index].discount) / 100)).toInt().toString()}",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              )
                            ],
                          )
                        ]),
                      );
                    },
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.75,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(29)),
                        color: Colors.greenAccent),
                    width: MediaQuery.of(context).size.width * 0.99,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(29)),
                              color: Colors.redAccent),
                          child: Text("Checkout",
                              style: TextStyle(color: Colors.white)),
                        ),
                        Flex(
                          direction: Axis.horizontal,
                          children: [
                            Text("Total"),
                            SizedBox(
                              width: 10,
                            ),
                            Text("NPR. ${Total.toInt()}"),
                            IconButton(
                                icon: Icon(Icons.arrow_circle_up_sharp),
                                onPressed: null)
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ]);
            } else if (state is CartLoading) {
              return Container(
                height: size.height * 0.3,
                child: Center(child: CircularProgressIndicator()),
              );
            } else {
              return Container(
                height: 0,
              );
            }
          })),
    );
  }
}
