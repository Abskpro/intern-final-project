import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/screens/wishListScreen/components/body.dart';
import 'package:intern_project/services/repository.dart';

class WishListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider<WishListRepository>(
      create: (context) => WishListRepository(),
      child: SafeArea(
        child: Scaffold(
          body: Body(),
        ),
      ),
    );
  }
}
