import 'package:flutter/material.dart';
import 'package:intern_project/bloc/cart/cart_event.dart';
import 'package:intern_project/bloc/wishlist/wish_list_event.dart';
import 'package:intern_project/widgets/buttons/product_button.dart';

class ProductCard extends StatefulWidget {
  final product;
  final bloc1;
  final bloc2;
  ProductCard({this.product, this.bloc1, this.bloc2});
  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  void wishlistDelete(String id) {
    print(id);
    widget.bloc2.add(WishListDeletePressed(pid: id));
  }

  void addToCart(String id) {
    print(id);
    widget.bloc1.add(CartButtonPressedEvent(pid: id));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.white,
      ),
      margin: EdgeInsets.fromLTRB(10, 0, 0, 10),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(children: [
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1.0, color: Colors.grey),
                    ),
                    color: Colors.white,
                  ),
                  height: 100,
                  child: Image.network(
                    widget.product.image[0]["128"],
                    fit: BoxFit.contain,
                  ),
                ),
                Divider(),
                Column(
                  children: [
                    Text(widget.product.label,
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.w600)),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(widget.product.manufacturingCompany,
                          style: TextStyle(fontSize: 10)),
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "NPR. ${widget.product.price.toString()}",
                          style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                              decoration: TextDecoration.lineThrough),
                        ),
                        Text(
                          "NPR. ${(widget.product.price - ((widget.product.price * widget.product.discount) / 100)).toInt().toString()}",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ProductBtn(
                          text: "Remove",
                          color: Colors.redAccent,
                          width: 0.18,
                          id: widget.product.productId,
                          height: 0.04,
                          fontSize: 8,
                          press: wishlistDelete,
                        ),
                        ProductBtn(
                          text: "Add to cart",
                          color: Colors.greenAccent[400],
                          width: 0.18,
                          id: widget.product.productId,
                          height: 0.04,
                          fontSize: 8,
                          press: addToCart,
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
            Positioned(
              top: 10,
              child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: Colors.blue,
                  ),
                  width: 50,
                  child: Text("-${widget.product.discount}%")),
            )
          ]),
        ),
      ),
    );
  }
}
