import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intern_project/bloc/cart/cart_bloc.dart';
import 'package:intern_project/bloc/cart/cart_state.dart';
import 'package:intern_project/bloc/wishlist/wish_list_bloc.dart';
import 'package:intern_project/bloc/wishlist/wish_list_event.dart';
import 'package:intern_project/bloc/wishlist/wish_list_state.dart';
import 'package:intern_project/screens/wishListScreen/components/background.dart';
import 'package:intern_project/screens/wishListScreen/components/product.dart';
import 'package:intern_project/services/repository.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  WishListBloc _wishListBloc;
  CartBloc _cartBloc;

  void initState() {
    super.initState();
    _cartBloc = CartBloc(cartRepository: context.read<CartRepository>());
    _wishListBloc =
        WishListBloc(wishListRepository: context.read<WishListRepository>());
    _wishListBloc.add(WishListLoadEvent());
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: MultiBlocProvider(
        providers: [
          BlocProvider<WishListBloc>(
            create: (BuildContext context) => _wishListBloc,
          ),
          BlocProvider<CartBloc>(
            create: (BuildContext context) => _cartBloc,
          )
        ],
        child: MultiBlocListener(
          listeners: [
            BlocListener<CartBloc, CartState>(
              listener: (BuildContext context, state) {
                if (state is CartStateFailed) {
                  Fluttertoast.showToast(
                    msg: state.message,
                    toastLength: Toast.LENGTH_SHORT,
                    timeInSecForIosWeb: 2,
                    backgroundColor: Colors.tealAccent,
                    textColor: Colors.white,
                    fontSize: 18.0,
                  );
                }
                if (state is CartStateSuccess) {
                  Fluttertoast.showToast(
                    msg: state.message,
                    toastLength: Toast.LENGTH_SHORT,
                    timeInSecForIosWeb: 2,
                    backgroundColor: Colors.tealAccent,
                    textColor: Colors.white,
                    fontSize: 18.0,
                  );
                }
              },
            ),
            BlocListener<WishListBloc, WishListState>(
                listener: (BuildContext context, state) {
              if (state is WishListDeleteFailed) {
                Fluttertoast.showToast(
                  msg: state.message,
                  toastLength: Toast.LENGTH_SHORT,
                  timeInSecForIosWeb: 2,
                  backgroundColor: Colors.tealAccent,
                  textColor: Colors.white,
                  fontSize: 18.0,
                );
              }
            })
          ],
          child: BlocConsumer<WishListBloc, WishListState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is WishListLoadSuccess) {
                  return Container(
                    height: MediaQuery.of(context).size.height * 0.8,
                    child: GridView.builder(
                        itemCount: state.banner.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 10,
                            childAspectRatio: 0.8),
                        itemBuilder: (context, index) => ProductCard(
                            product: state.banner[index],
                            bloc1: _cartBloc,
                            bloc2: _wishListBloc)),
                  );
                } else if (state is WishListDeleteSuccess) {
                  return Container(
                    height: MediaQuery.of(context).size.height * 0.8,
                    child: GridView.builder(
                        itemCount: state.banner.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 10,
                            childAspectRatio: 0.8),
                        itemBuilder: (context, index) => ProductCard(
                            product: state.banner[index],
                            bloc1: _cartBloc,
                            bloc2: _wishListBloc)),
                  );
                } else {
                  return Container(
                    height: size.height * 0.3,
                    child: Center(child: CircularProgressIndicator()),
                  );
                }
              }),
        ),
      ),
    );
  }
}
