import 'package:flutter/material.dart';
import 'package:intern_project/widgets/appbar/app_bar.dart';

class Background extends StatefulWidget {
  final Widget child;
  const Background({this.child});
  @override
  _BackgroundState createState() => _BackgroundState();
}

class _BackgroundState extends State<Background> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.grey[300],
      child: CustomScrollView(
        slivers: <Widget>[
          AppBarr(),
          // _wishList(context),
          SliverList(
            delegate: SliverChildListDelegate([widget.child]),
          )
        ],
      ),
    );
  }

  // Widget _wishList(BuildContext context) {
  //   DefaultTabController(
  //     length: 2,
  //     child: MaterialApp(
  //       home: Scaffold(
  //         appBar: AppBar(
  //           bottom: TabBar(
  //             onTap: (index) {
  //               // Tab index when user select it, it start from zero
  //             },
  //             tabs: [
  //               Tab(icon: Icon(Icons.favorite)),
  //               Tab(icon: Icon(Icons.list)),
  //             ],
  //           ),
  //           title: Text('Tabs Demo'),
  //         ),
  //         body: TabBarView(
  //           children: [
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }
}
