import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/screens/verifyScreen/components/body.dart';
import 'package:intern_project/services/otp_api_provider.dart';
import 'package:intern_project/services/repository.dart';

class VerifyScreen extends StatelessWidget {
  final String number;

  VerifyScreen({this.number});
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider<OtpRepository>(
      create: (context) => OtpRepository(),
      child: SafeArea(
        child: Scaffold(
          body: Body(number: number),
        ),
      ),
    );
  }
}
