import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intern_project/bloc/otp/otp_bloc.dart';
import 'package:intern_project/bloc/otp/otp_event.dart';
import 'package:intern_project/bloc/otp/otp_state.dart';
import 'package:intern_project/screens/loginScreen/login_screen.dart';
import 'package:intern_project/screens/passwordResetScreen/password_reset_screen.dart';
import 'package:intern_project/screens/verifyScreen/components/background.dart';
import 'package:intern_project/services/repository.dart';
import 'package:intern_project/widgets/buttons/form_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class Body extends StatefulWidget {
  String number;
  Body({this.number});
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  String otpCode = "";

  OtpBloc _otpBloc;

  void initState() {
    super.initState();

    _otpBloc = OtpBloc(otpRepository: context.read<OtpRepository>());
  }

  void submit() {
    print("afsadfj");
    _otpBloc.add(OtpSubmitPressed(number: widget.number));
  }

  bool validateOpt({checkEmpty: false}) {
    return (otpCode.isEmpty && !checkEmpty) || otpCode.length == 6;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OtpBloc>(
      create: (BuildContext context) => _otpBloc,
      child: Background(
        child: BlocConsumer<OtpBloc, OtpState>(listener: (context, state) {
          if (state is OtpFailState) {
            Fluttertoast.showToast(
              msg: state.message,
              toastLength: Toast.LENGTH_SHORT,
              timeInSecForIosWeb: 2,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 18.0,
            );
          }
          if (state is OtpSuccessState) {
            Fluttertoast.showToast(
              msg: state.message,
              toastLength: Toast.LENGTH_SHORT,
              timeInSecForIosWeb: 2,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 18.0,
            );
            Navigator.pop(context);
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return LoginScreen();
            }));
          }
          if (state is OtpLoadingState) {
            Scaffold.of(context).showSnackBar(SnackBar(
                content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Verifying"),
                CircularProgressIndicator(),
              ],
            )));
          }
        }, builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Text(
                  'Enter 6 digit Otp Code',
                  style: TextStyle(fontSize: 20.0),
                ),
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: PinCodeTextField(
                    appContext: context,
                    length: 6,
                    onChanged: (value) {
                      setState(() {
                        otpCode = value;
                      });
                    },
                    pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderRadius: BorderRadius.circular(5),
                        fieldHeight: 50,
                        fieldWidth: 40,
                        inactiveColor: Colors.purpleAccent,
                        activeColor: Colors.orangeAccent),
                    keyboardType: TextInputType.phone,
                  ),
                ),
                FormBtn(
                    validateNumber: validateOpt,
                    width: 0.4,
                    text: "Verify",
                    color: Colors.greenAccent,
                    screen: "verify",
                    press: submit)
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget _forgotPassword(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return PasswordReset();
          }));
        },
        child: Text(
          "Forgot Password",
          style: TextStyle(color: Color(0xFF9E9E9E)),
        ));
  }
}
