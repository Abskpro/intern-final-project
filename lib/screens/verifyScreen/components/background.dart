import 'package:flutter/material.dart';
import 'package:intern_project/widgets/appbar/form_app_bar.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({this.child});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        height: size.height,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                width: size.width,
                height: size.height / 2,
                decoration: BoxDecoration(
                  color: Colors.greenAccent,
                ),
              ),
            ),
            Positioned(top: size.height * 0.4, child: child),
            Positioned(
              top: 50,
              child: Container(
                  // margin: EdgeInsets.only(top: 30),
                  height: 150,
                  child: Image.asset("assets/images/logo.png")),
            )
          ],
        ),
      ),
    );
  }
}

class BackgroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0, size.height * 0.7);
    var controlPoint = Offset(250, size.height);
    var endPoint = Offset(size.width / 3, size.height);
    path.quadraticBezierTo(
        controlPoint.dx, controlPoint.dy, endPoint.dx * 3, endPoint.dy * 0.5);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    // path.lineTo(0, size.height * 0.75);
    // path.quadraticBezierTo(350, size.width, size.height * 1.3, 0);
    // path.lineTo(size.width, size.height);
    // path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
