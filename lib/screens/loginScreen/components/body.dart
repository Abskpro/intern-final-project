import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intern_project/bloc/login/login_bloc.dart';
import 'package:intern_project/bloc/login/login_event.dart';
import 'package:intern_project/bloc/login/login_state.dart';
import 'package:intern_project/screens/homeScreen/home_screen.dart';
import 'package:intern_project/screens/loginScreen/components/background.dart';
import 'package:intern_project/screens/passwordResetScreen/password_reset_screen.dart';
import 'package:intern_project/services/repository.dart';
import 'package:intern_project/widgets/buttons/form_button.dart';
import 'package:intern_project/widgets/inputField/number_input.dart';
import 'package:intern_project/widgets/inputField/password_input.dart';
import 'package:intern_project/widgets/redirect/signup.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  String number = "";
  String password = "";
  TextEditingController numberController;
  TextEditingController passwordController;

  LoginBloc _loginBloc;

  void initState() {
    super.initState();
    numberController = TextEditingController();
    passwordController = TextEditingController();
    numberController.addListener(_updateNumber);
    passwordController.addListener(_updatePassword);

    _loginBloc = LoginBloc(loginRepository: context.read<LoginRepository>());
  }

  _updateNumber() {
    print('validating number');
    setState(() {
      number = numberController.text;
    });
  }

  _updatePassword() {
    print('validating number');
    setState(() {
      password = passwordController.text;
    });
  }

  bool validateNumber({checkEmpty: false}) {
    return (number.isEmpty && !checkEmpty) || number.length == 10;
  }

  bool validatePassword({checkEmpty: false}) {
    print('validating password');
    return (password.isEmpty && !checkEmpty) || password.length > 8;
  }

  void validate() async {
    print("submitting....");
    _loginBloc.add(LoginButtonPressed(
        number: numberController.text, password: passwordController.text));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>(
      create: (BuildContext context) => _loginBloc,
      child: Background(
        child: BlocConsumer<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is LoginFailState) {
              Fluttertoast.showToast(
                msg: state.message,
                toastLength: Toast.LENGTH_SHORT,
                timeInSecForIosWeb: 2,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 18.0,
              );
            }
            if (state is LoginSuccessState) {
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return HomeScreen();
              }));
            }
            if (state is LoginLoadingState) {
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Logging In"),
                  CircularProgressIndicator(),
                ],
              )));
            }
          },
          builder: (context, state) {
            return Container(
                alignment: Alignment.center,
                width: double.infinity,
                child: Form(
                  key: formkey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 140,
                      ),
                      Container(
                        height: 230,
                        child: Image.asset("assets/images/form-bg.png",
                            fit: BoxFit.contain),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      NumberInput(
                        controller: numberController,
                      ),
                      PasswordInput(
                        text: "Password",
                        controller: passwordController,
                      ),
                      FormBtn(
                          width: 0.42,
                          text: "Login",
                          press: validate,
                          validateNumber: validateNumber,
                          color: Colors.lightGreen,
                          validatePassword: validatePassword,
                          screen: "login"),
                      SizedBox(
                        height: 10,
                      ),
                      _forgotPassword(context),
                      SizedBox(
                        height: 10,
                      ),
                      SignUp(
                        text: "Sign Up",
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ));
          },
        ),
      ),
    );
  }

  Widget _forgotPassword(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return PasswordReset();
          }));
        },
        child: Text(
          "Forgot Password",
          style: TextStyle(color: Color(0xFF9E9E9E)),
        ));
  }
}
