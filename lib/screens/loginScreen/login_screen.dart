import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/screens/loginScreen/components/body.dart';
import 'package:intern_project/services/repository.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider<LoginRepository>(
      create: (context) => LoginRepository(),
      child: SafeArea(
        child: Scaffold(
          body: Body(),
        ),
      ),
    );
  }
}
