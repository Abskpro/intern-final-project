import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intern_project/screens/homeScreen/components/background.dart';
import 'package:intern_project/screens/homeScreen/home_screen.dart';
import 'package:intern_project/screens/loginScreen/login_screen.dart';
import 'package:intern_project/screens/profileScreen/profile_screen.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var storage = FlutterSecureStorage();

  Future<String> chechToken() async {
    var token = await storage.read(key: "access-token");
    print('token $token');
    if (token != null) {
      return "asdfasd";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.white,
            child: ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: ListTile.divideTiles(
                context: context,
                tiles: [
                  FutureBuilder(
                      future: chechToken(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    padding: EdgeInsets.only(left: 20),
                                    child: Text("Account Settings",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold))),
                                ListTile(
                                    title: Text("Personal Information"),
                                    leading: Icon(Icons.supervised_user_circle),
                                    onTap: () {
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return ProfileScreen();
                                      }));
                                    }),
                              ],
                            ),
                          );
                        } else {
                          return Container(
                            height: 0,
                          );
                        }
                      }),
                  FutureBuilder(
                      future: chechToken(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    padding: EdgeInsets.only(left: 20),
                                    child: Text("Security Settings",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold))),
                                ListTile(
                                    title: Text("LogOut"),
                                    leading: Icon(Icons.logout),
                                    onTap: () async {
                                      await storage.deleteAll().then((value) {
                                        print("token deleted");
                                        setState(() {});
                                      });
                                      Navigator.pop(context);
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return HomeScreen();
                                      }));
                                    }),
                              ],
                            ),
                          );
                        } else {
                          return Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 20),
                                    child: Text("Security Settings",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold))),
                                ListTile(
                                    title: Text("Login"),
                                    leading: Icon(Icons.login),
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return LoginScreen();
                                      }));
                                    }),
                              ],
                            ),
                          );
                        }
                      })
                ],
              ).toList(),
            ),
          )
        ],
      ),
    );
  }
}
