import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/bloc/auth/auth_bloc.dart';
import 'package:intern_project/bloc/auth/auth_event.dart';
import 'package:intern_project/screens/homeScreen/home_screen.dart';
import 'package:intern_project/services/repository.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  BannerRepository _repository = BannerRepository();

  runApp(MyApp(
    bannerRepository: _repository,
  ));
}

class MyApp extends StatelessWidget {
  final BannerRepository bannerRepository;
  MyApp({this.bannerRepository});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          AuthBloc(bannerRepository: bannerRepository)..add(AppStartedEvent()),
      child: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<BannerRepository>(
                create: (context) => BannerRepository()),
            RepositoryProvider<BrandRepository>(
                create: (context) => BrandRepository()),
            RepositoryProvider<BannerMeemeeRepository>(
                create: (context) => BannerMeemeeRepository()),
            RepositoryProvider<ProductRepository>(
                create: (context) => ProductRepository()),
            RepositoryProvider<CartRepository>(
                create: (context) => CartRepository()),
            RepositoryProvider<WishListRepository>(
                create: (context) => WishListRepository()),
          ],
          child: MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: HomeScreen(),
          )),
    );
  }
}
