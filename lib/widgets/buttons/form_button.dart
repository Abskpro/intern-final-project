import 'package:flutter/material.dart';
import 'package:intern_project/constants/colour.dart';

class FormBtn extends StatefulWidget {
  final Function press;
  final Function onChanged;
  final Function validateNumber;
  final Function validatePassword;
  final Function validateConfirmPassword;
  final Function validateCheckBox;
  final Function validateDate;
  final Color color;
  final String screen;
  final String text;
  final double width;

  const FormBtn(
      {Key key,
      this.onChanged,
      this.press,
      this.validateNumber,
      this.validatePassword,
      this.validateConfirmPassword,
      this.validateCheckBox,
      this.validateDate,
      this.text,
      this.color,
      this.screen,
      this.width});
  @override
  _FormBtnState createState() => _FormBtnState();
}

class _FormBtnState extends State<FormBtn> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * widget.width,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: RaisedButton(
            color: widget.color,
            onPressed: _checker(),
            padding: EdgeInsets.symmetric(vertical: 13),
            child: Text(
              widget.text,
              style: TextStyle(color: btnTextColor),
            )),
      ),
    );
  }

  _checker() {
    if (widget.screen == "login") {
      return (widget.validateNumber(checkEmpty: true) &&
              widget.validatePassword(checkEmpty: true)
          ? widget.press
          : null);
    }
    if (widget.screen == "signup") {
      return (widget.validateNumber(checkEmpty: true) &&
              widget.validatePassword(checkEmpty: true) &&
              widget.validateConfirmPassword(checkEmpty: true) &&
              widget.validateDate(checkEmpty: true) &&
              widget.validateCheckBox()
          ? widget.press
          : null);
    }
    if (widget.screen == "reset") {
      return (widget.validateNumber(checkEmpty: true) ? widget.press : null);
    }

    if (widget.screen == "profile") {
      return widget.press;
    }

    if (widget.screen == "verify") {
      return (widget.validateNumber(checkEmpty: true) ? widget.press : null);
    }

    if (widget.screen == "banner" ||
        widget.screen == "featured" ||
        widget.screen == "meemee") {
      return widget.press;
    }
  }
}
