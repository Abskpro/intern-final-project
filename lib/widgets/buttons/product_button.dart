import 'package:flutter/material.dart';
import 'package:intern_project/constants/colour.dart';

class ProductBtn extends StatefulWidget {
  final Function press;
  final Function onChanged;
  final Color color;
  final String screen;
  final String text;
  final double width;
  final double height;
  final double fontSize;
  final String id;

  const ProductBtn(
      {Key key,
      this.onChanged,
      this.press,
      this.text,
      this.color,
      this.screen,
      this.height,
      this.fontSize,
      this.id,
      this.width});
  @override
  _ProductBtnState createState() => _ProductBtnState();
}

class _ProductBtnState extends State<ProductBtn> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * widget.width,
      height: size.height * widget.height,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: RaisedButton(
            color: widget.color,
            onPressed: () => widget.press(widget.id),
            child: Text(
              widget.text,
              style: TextStyle(color: btnTextColor, fontSize: widget.fontSize),
            )),
      ),
    );
  }
}
