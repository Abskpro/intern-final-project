import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intern_project/screens/cartScreen/cart_screen.dart';
import 'package:intern_project/widgets/appbar/search_bar.dart';

class AppBarr extends StatefulWidget {
  @override
  _AppBarrState createState() => _AppBarrState();
}

class _AppBarrState extends State<AppBarr> {
  var storage = FlutterSecureStorage();

  void initState() {
    super.initState();
  }

  Future<String> chechToken() async {
    var token = await storage.read(key: "access-token");
    print('token $token');
    if (token != null) {
      return "asdfasd";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text("Home"),
        actions: <Widget>[
          FutureBuilder(
            future: chechToken(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Row(children: [
                  Visibility(
                    visible: storage.read(key: "access-token") != null,
                    child: IconButton(
                        icon: Icon(Icons.notifications),
                        onPressed: () async {}),
                  ),
                  Visibility(
                    visible: storage.read(key: "access-token") != null,
                    child: IconButton(
                      icon: Icon(Icons.shopping_cart),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return CartScreen();
                        }));
                      },
                    ),
                  )
                ]);
              } else {
                return Container(
                  height: 0,
                );
              }
            },
          ),
          // ),
        ],
        pinned: false,
        backgroundColor: Colors.teal[400],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
        floating: true,
        expandedHeight: 95,
        flexibleSpace: FlexibleSpaceBar(
          background: SearchBar(),
        ));
  }
}
