import 'package:flutter/material.dart';

class FormAppBar extends StatefulWidget {
  final Color bgColor;
  const FormAppBar({this.bgColor});
  @override
  _FormAppBarState createState() => _FormAppBarState();
}

class _FormAppBarState extends State<FormAppBar> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        color: widget.bgColor,
        width: double.infinity,
        child:
            Align(alignment: Alignment.topLeft, child: Icon(Icons.arrow_back)),
      ),
    );
  }
}
