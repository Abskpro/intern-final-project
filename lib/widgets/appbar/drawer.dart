import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intern_project/screens/homeScreen/home_screen.dart';
import 'package:intern_project/screens/loginScreen/login_screen.dart';
import 'package:intern_project/screens/settingScreen/setting_screen.dart';
import 'package:intern_project/screens/signupScreen/signup_screen.dart';
import 'package:intern_project/screens/verifyScreen/verify_screen.dart';
import 'package:intern_project/screens/wishListScreen/wish_list_screen.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  var storage = FlutterSecureStorage();

  void initState() {
    super.initState();
  }

  Future<String> chechToken() async {
    var token = await storage.read(key: "access-token");
    print('token $token');
    if (token != null) {
      return "asdfasd";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(topRight: Radius.circular(50)),
      child: Drawer(
        child: ListView(
          children: [
            Container(
              height: 140,
              child: DrawerHeader(
                padding: EdgeInsets.all(20),
                child: Image.asset(
                  "assets/images/logo.png",
                  fit: BoxFit.contain,
                ),
              ),
            ),
            ListTile(
              title: Text("Settings"),
              leading: Icon(Icons.settings),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return SettingScreen();
                }));
              },
            ),
            ListTile(
              title: Text("Feedback"),
              leading: Icon(Icons.feedback),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return VerifyScreen();
                }));
              },
            ),
            FutureBuilder(
              future: chechToken(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListTile(
                    title: Text("Saved items"),
                    leading: Icon(Icons.save_alt),
                    onTap: () async {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return WishListScreen();
                      }));
                    },
                  );
                } else {
                  return Container(height: 0);
                }
              },
            ),

            ///
            FutureBuilder(
              future: chechToken(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListTile(
                    title: Text("Logout from Jeevee"),
                    leading: Icon(Icons.logout),
                    onTap: () async {
                      await storage.deleteAll().then((value) {
                        print("token deleted");
                        setState(() {});
                      });
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return HomeScreen();
                      }));
                    },
                  );
                } else {
                  return ListTile(
                    title: Text("Login to Jeevee"),
                    leading: Icon(Icons.login),
                    onTap: () {
                      // Navigator.pop(context);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return LoginScreen();
                      }));
                    },
                  );
                }
              },
            ),
            FutureBuilder(
              future: chechToken(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Container();
                } else {
                  return ListTile(
                    title: Text("Join Jeevee"),
                    leading: Icon(Icons.supervised_user_circle),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return SignUpScreen();
                      }));
                    },
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
