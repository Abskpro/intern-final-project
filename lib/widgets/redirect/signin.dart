import 'package:flutter/material.dart';
import 'package:intern_project/screens/loginScreen/login_screen.dart';

class SignIn extends StatelessWidget {
  final String text;
  const SignIn({this.text});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10),
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return LoginScreen();
          }));
        },
        child: Text(
          text,
          style: TextStyle(color: Color(0xFF9E9E9E)),
        ),
      ),
    );
  }
}
