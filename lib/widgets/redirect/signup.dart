import 'package:flutter/material.dart';
import 'package:intern_project/screens/signupScreen/signup_screen.dart';

class SignUp extends StatelessWidget {
  final String text;
  const SignUp({this.text});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Don't have an Account?"),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return SignUpScreen();
              }));
            },
            child: Text(
              text,
              style: TextStyle(color: Colors.greenAccent),
            ),
          ),
        )
      ],
    );
  }
}
