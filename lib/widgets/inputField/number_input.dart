import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intern_project/widgets/inputField/text_field_container.dart';

class NumberInput extends StatefulWidget {
  final TextEditingController controller;
  final Function validate;
  final Function onChanged;
  const NumberInput({Key key, this.controller, this.validate, this.onChanged});
  @override
  _NumberInputState createState() => _NumberInputState();
}

class _NumberInputState extends State<NumberInput> {
  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        controller: widget.controller,
        validator: (value) {
          if (value.length < 10 && value.length >= 1) {
            return "Number must be valid";
          }
          return null;
        },
        inputFormatters: [
          LengthLimitingTextInputFormatter(10),
        ],
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
            hintText: "Mobile Number *",
            hintStyle: TextStyle(color: Colors.grey),
            contentPadding: EdgeInsets.only(top: 5, left: 15),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(29))),
      ),
    );
  }
}
