import 'package:flutter/material.dart';
import 'package:intern_project/constants/colour.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({Key key, this.child});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        margin: EdgeInsets.symmetric(vertical: 9),
        // padding: EdgeInsets.symmetric(horizontal: 20),
        // width: size.width * 0.8,
        // height: 45,
        // decoration: BoxDecoration(
        //     borderRadius: BorderRadius.circular(29),
        // border: Border.all(color: Colors.grey)),
        child: child);
  }
}
