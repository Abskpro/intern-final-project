import 'package:flutter/material.dart';
import 'package:intern_project/widgets/inputField/text_field_container.dart';

class PasswordInput extends StatefulWidget {
  final TextEditingController controller;
  final String type;
  final String text;
  final Function onChanged;
  final String screen;
  const PasswordInput(
      {Key key,
      this.controller,
      this.type,
      this.onChanged,
      this.screen,
      this.text});

  @override
  _PasswordInputState createState() => _PasswordInputState();
}

class _PasswordInputState extends State<PasswordInput> {
  bool visibility;
  void initState() {
    super.initState();
    visibility = true;
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        controller: widget.controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        obscureText: visibility,
        validator: (value) {
          if (value.length < 8 && value.length > 1) {
            return "password must of length 8";
          }
          return null;
        },
        decoration: InputDecoration(
            hintText: widget.text,
            hintStyle: TextStyle(color: Colors.grey),
            contentPadding: EdgeInsets.only(top: 5, left: 15),
            suffixIcon: InkWell(
              onTap: () {
                setState(() {
                  visibility = !visibility;
                });
              },
              child: Icon(visibility ? Icons.visibility_off : Icons.visibility),
            ),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(29))),
      ),
    );
  }
}
