import 'package:flutter/material.dart';

class DropDown extends StatefulWidget {
  final String dropdownValue;
  final List<String> item;
  final Function onChanged;
  final String type;
  DropDown({this.dropdownValue, this.item, this.onChanged, this.type});

  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropDown> {
  String selected;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.45,
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        border: Border.all(),
        borderRadius: BorderRadius.all(Radius.circular(29)),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
            value: selected == null ? widget.dropdownValue : selected,
            isExpanded: true,
            focusColor: Colors.red,
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 20,
            elevation: 10,
            style: TextStyle(color: Colors.black, fontSize: 15),
            onChanged: (String newVal) {
              widget.onChanged(newVal, widget.type);
              setState(() {
                selected = newVal;
              });
            },
            items: widget.item.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                  ));
            }).toList()),
      ),
    );
  }
}
