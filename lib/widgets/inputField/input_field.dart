import 'package:flutter/material.dart';
import 'package:intern_project/widgets/inputField/text_field_container.dart';

class TextInputField extends StatefulWidget {
  final TextEditingController controller;
  final Function validate;
  final Function onChanged;
  final String hintText;
  final String input;
  const TextInputField(
      {Key key,
      this.controller,
      this.validate,
      this.input,
      this.onChanged,
      this.hintText});
  @override
  _TextInputFieldState createState() => _TextInputFieldState();
}

class _TextInputFieldState extends State<TextInputField> {
  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        // controller: widget.controller,
        onChanged: (value) {
          widget.onChanged(value, widget.input);
        },
        decoration: InputDecoration(
            hintText: widget.hintText,
            hintStyle: TextStyle(color: Colors.grey),
            contentPadding: EdgeInsets.only(top: 5, left: 15),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(29))),
      ),
    );
  }
}
