import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF69F0AE);
const kPrimaryLightColor = Color(0xFFF1E6FF);
const btnTextColor = Color(0xFFFFFFFF);
const jeeveColor = Color(0xFF1DE9B6);
const btnDanger = Colors.red;
const secondaryColor = Colors.tealAccent;
