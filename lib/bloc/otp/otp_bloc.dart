import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/otp/otp_event.dart';
import 'package:intern_project/bloc/otp/otp_state.dart';
import 'package:intern_project/services/repository.dart';

class OtpBloc extends Bloc<OtpEvent, OtpState> {
  OtpRepository otpRepository;

  OtpBloc({@required OtpRepository otpRepository}) : super(null) {
    this.otpRepository = otpRepository;
  }

  Stream<OtpState> mapEventToState(OtpEvent event) async* {
    if (event is OtpSubmitPressed) {
      yield OtpLoadingState();
      try {
        var message = await otpRepository.submitOtp(event.number);
        yield OtpSuccessState(message);
      } catch (e) {
        // print(e.toString());
        yield OtpFailState(e.toString());
      }
    }
  }
}
