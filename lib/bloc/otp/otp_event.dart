abstract class OtpEvent {}

class OtpSubmitPressed extends OtpEvent {
  String number;

  OtpSubmitPressed({this.number});
}
