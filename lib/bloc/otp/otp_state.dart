abstract class OtpState {}

class OtpInitialState extends OtpState {}

class OtpLoadingState extends OtpState {}

class OtpSuccessState extends OtpState {
  String message;
  OtpSuccessState(this.message);
}

class OtpFailState extends OtpState {
  String message;

  OtpFailState(this.message);
}
