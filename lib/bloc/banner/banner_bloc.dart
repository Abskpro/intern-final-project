import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/banner/banner_event.dart';
import 'package:intern_project/bloc/banner/banner_state.dart';
import 'package:intern_project/services/repository.dart';

class BannerBloc extends Bloc<BannerEvent, BannerPageState> {
  BannerRepository bannerRepository;

  BannerBloc({@required BannerRepository bannerRepository}) : super(null) {
    this.bannerRepository = bannerRepository;
  }

  BannerPageState get initialState => BannerInitialState();

  Stream<BannerPageState> mapEventToState(BannerEvent event) async* {
    if (event is BannerLoadEvent) {
      yield BannerLoadingState();
      try {
        var banner = await bannerRepository.fetchBanners();
        yield BannerLoadingSuccessState(banner);
      } catch (e) {
        yield BannerLoadingFailedState();
        print(e.toString());
      }
    }
  }
}
