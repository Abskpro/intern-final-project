import 'package:intern_project/models/banner_model.dart';

abstract class BannerPageState {}

class BannerInitialState extends BannerPageState {}

class BannerLoadingState extends BannerPageState {}

class BannerLoadingSuccessState extends BannerPageState {
  final List<Banners> banner;

  BannerLoadingSuccessState(this.banner);
}

class BannerLoadingFailedState extends BannerPageState {}
