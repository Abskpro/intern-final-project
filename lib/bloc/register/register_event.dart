abstract class RegisterEvent {}

class RegisterButtonPressed extends RegisterEvent {
  String number, password, date, role;

  RegisterButtonPressed({this.number, this.date, this.password, this.role});
}
