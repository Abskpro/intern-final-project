import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/login/login_event.dart';
import 'package:intern_project/bloc/register/register_event.dart';
import 'package:intern_project/bloc/register/register_state.dart';
import 'package:intern_project/services/repository.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterRepository registerRepository;

  RegisterBloc({@required RegisterRepository registerRepository})
      : super(null) {
    this.registerRepository = registerRepository;
  }

  RegisterState get initialState => RegisterInitalState();

  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterButtonPressed) {
      yield RegisterLoadingState();
      try {
        var user = await registerRepository.signUpUser(
            event.number, event.date, event.password, event.role);
        yield RegisterSuccessState(user);
        print(user);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
