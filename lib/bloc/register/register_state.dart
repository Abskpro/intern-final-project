import 'package:intern_project/models/register_model.dart';

abstract class RegisterState {}

class RegisterInitalState extends RegisterState {}

class RegisterLoadingState extends RegisterState {}

class RegisterSuccessState extends RegisterState {
  Register user;
  RegisterSuccessState(this.user);
}

class RegisterFailState extends RegisterState {
  String message;

  RegisterFailState(this.message);
}
