import 'package:intern_project/models/banner_meemeee_model.dart';

abstract class MeemeePageState {}

class MeeemeeInitialState extends MeemeePageState {}

class MeemeeLoadingState extends MeemeePageState {}

class MeemeeSuccessState extends MeemeePageState {
  final List<Data> banner;

  MeemeeSuccessState(this.banner);
}

class MeemeeLoadFailedState extends MeemeePageState {}
