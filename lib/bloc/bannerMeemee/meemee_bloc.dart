import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/bannerMeemee/meemee_event.dart';
import 'package:intern_project/bloc/bannerMeemee/meemee_state.dart';
import 'package:intern_project/services/repository.dart';

class MeemeeBloc extends Bloc<MeemeeEvent, MeemeePageState> {
  BannerMeemeeRepository bannerMeemeeRepository;

  MeemeeBloc({@required BannerMeemeeRepository bannerMeemeeRepository})
      : super(null) {
    this.bannerMeemeeRepository = bannerMeemeeRepository;
  }

  Stream<MeemeePageState> mapEventToState(MeemeeEvent event) async* {
    if (event is MeemeeLoadEvent) {
      yield MeemeeLoadingState();
      try {
        var banner = await bannerMeemeeRepository.fetchMeemee();
        yield MeemeeSuccessState(banner);
      } catch (e) {
        print(e.toString());
        yield MeemeeLoadFailedState();
      }
    }
  }
}
