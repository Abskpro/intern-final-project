import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/updateProfile/profile_update_event.dart';
import 'package:intern_project/bloc/updateProfile/profile_update_state.dart';
import 'package:intern_project/services/repository.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileRepository profileRepository;

  ProfileBloc({@required ProfileRepository profileRepository}) : super(null) {
    this.profileRepository = profileRepository;
  }

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is UpdateButtonPressed) {
      yield ProfileUpdateLoadingState();
      try {
        var user = await profileRepository.updatePofile(
            event.firstName, event.lastName, event.bloodGroup, event.gender);
        yield ProfileUpdateSuccesssState(user);
      } catch (e) {
        print(e.toString());
      }
    }
    if (event is ProfileLoadEvent) {
      print("yooo hoo bloc reached");
      yield ProfileLoadingState();
      try {
        var user = await profileRepository.fetchUserInfo();
        yield ProfileLoadSuccessState(user);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
