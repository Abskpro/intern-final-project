import 'package:intern_project/models/profile.model.dart';

abstract class ProfileState {}

class ProfileInitialState extends ProfileState {}

class ProfileLoadingState extends ProfileState {}

class ProfileUpdateLoadingState extends ProfileState {}

class ProfileUpdateSuccesssState extends ProfileState {
  UserProfile user;
  ProfileUpdateSuccesssState(this.user);
}

class ProfileLoadSuccessState extends ProfileState {
  UserProfile user;
  ProfileLoadSuccessState(this.user);
}

class ProfileUpdateFailState extends ProfileState {
  String message;

  ProfileUpdateFailState(this.message);
}

class ProfileLoadFailState extends ProfileState {
  String message;

  ProfileLoadFailState(this.message);
}
