abstract class ProfileEvent {}

class UpdateButtonPressed extends ProfileEvent {
  String firstName, lastName, bloodGroup, gender;

  UpdateButtonPressed(
      {this.firstName, this.lastName, this.bloodGroup, this.gender});
}

class ProfileLoadEvent extends ProfileEvent {}
