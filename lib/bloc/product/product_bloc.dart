import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:intern_project/bloc/product/product_event.dart';
import 'package:intern_project/bloc/product/product_state.dart';
import 'package:intern_project/services/repository.dart';

class ProductBloc extends Bloc<ProductEvent, ProductPageState> {
  ProductRepository productRepository;

  ProductBloc({@required ProductRepository productRepository}) : super(null) {
    this.productRepository = productRepository;
  }

  Stream<ProductPageState> mapEventToState(ProductEvent event) async* {
    if (event is ProductLoadEvent) {
      try {
        if (event.product == "skin") {
          yield SkinProductLoadingState();
          var banner = await productRepository.fetchProducts(event.id);
          yield SkinProductSuccessState(banner);
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }
}

class BabyProductBloc extends Bloc<ProductEvent, BabyProductPageState> {
  ProductRepository productRepository;

  BabyProductBloc({@required ProductRepository productRepository})
      : super(null) {
    this.productRepository = productRepository;
  }

  Stream<BabyProductPageState> mapEventToState(ProductEvent event) async* {
    if (event is ProductLoadEvent) {
      try {
        if (event.product == "baby") {
          yield BabyProductLoadingState();
          var banner = await productRepository.fetchProducts(event.id);
          yield BabyProductSuccessState(banner);
        }
        // print("yo this is banner ${banner.length}");
      } catch (e) {
        print(e.toString());
      }
    }
  }
}

class MedicalProductBloc extends Bloc<ProductEvent, MedicalProductPageState> {
  ProductRepository productRepository;

  MedicalProductBloc({@required ProductRepository productRepository})
      : super(null) {
    this.productRepository = productRepository;
  }

  Stream<MedicalProductPageState> mapEventToState(ProductEvent event) async* {
    if (event is ProductLoadEvent) {
      try {
        if (event.product == "medical") {
          yield MedicalProductLoadingState();
          var banner = await productRepository.fetchProducts(event.id);
          yield MedicalDeviceProductSuccessState(banner);
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
