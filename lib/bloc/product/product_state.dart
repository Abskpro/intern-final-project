import 'package:intern_project/models/product_model.dart';

abstract class ProductPageState {}

abstract class BabyProductPageState {}

abstract class MedicalProductPageState {}

class ProductInitialState extends ProductPageState {}

class BabyProductInitalState extends BabyProductPageState {}

class MedicalProductInitalState extends MedicalProductPageState {}

class SkinProductLoadingState extends ProductPageState {}

class BabyProductLoadingState extends BabyProductPageState {}

class MedicalProductLoadingState extends MedicalProductPageState {}

class BabyProductSuccessState extends BabyProductPageState {
  final List<Datum> banner;

  BabyProductSuccessState(this.banner);
}

class SkinProductSuccessState extends ProductPageState {
  final List<Datum> banner;

  SkinProductSuccessState(this.banner);
}

class MedicalDeviceProductSuccessState extends MedicalProductPageState {
  final List<Datum> banner;

  MedicalDeviceProductSuccessState(this.banner);
}
