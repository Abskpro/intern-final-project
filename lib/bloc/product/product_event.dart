abstract class ProductEvent {}

class ProductLoadEvent extends ProductEvent {
  int id;
  String product;
  ProductLoadEvent({this.id, this.product});
}
