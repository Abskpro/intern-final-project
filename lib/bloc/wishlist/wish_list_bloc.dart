import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/wishlist/wish_list_event.dart';
import 'package:intern_project/bloc/wishlist/wish_list_state.dart';
import 'package:intern_project/services/repository.dart';

class WishListBloc extends Bloc<WishListEvent, WishListState> {
  WishListRepository wishListRepository;

  WishListBloc({@required WishListRepository wishListRepository})
      : super(null) {
    this.wishListRepository = wishListRepository;
  }

  Stream<WishListState> mapEventToState(WishListEvent event) async* {
    if (event is WishListButtonPressed) {
      yield WishListStateLoading();
      try {
        var banner = await wishListRepository.updateWishList(event.pid);
        yield WishListStateSuccess(message: banner);
      } catch (e) {
        yield WishListLoadFailed();
        print(e.toString());
      }
    }
    if (event is WishListLoadEvent) {
      yield WishListStateLoading();
      try {
        var banner = await wishListRepository.loadWishList();
        yield WishListLoadSuccess(banner: banner);
        print("asdfasdfasdfasfasd ... $banner");
      } catch (e) {
        yield WishListSavedFailed();
        print(e.toString());
      }
    }
    if (event is WishListDeletePressed) {
      try {
        var banner =
            await wishListRepository.deleteItem(event.pid).then((value) async {
          return await wishListRepository.loadWishList();
        });
        yield WishListDeleteSuccess(banner: banner);
      } catch (e) {
        yield WishListDeleteFailed();
        print(e.toString());
      }
    }
  }
}
