import 'package:intern_project/models/wish_list_model.dart';

abstract class WishListEvent {}

class WishListLoadEvent extends WishListEvent {}

class WishListButtonPressed extends WishListEvent {
  String pid;
  WishListButtonPressed({this.pid});
}

class WishListDeletePressed extends WishListEvent {
  String pid;
  WishListDeletePressed({this.pid});
}
