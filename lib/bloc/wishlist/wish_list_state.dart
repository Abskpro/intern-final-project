import 'package:intern_project/models/wish_list_model.dart';

abstract class WishListState {}

class WishListStateInitial extends WishListState {}

class WishListStateLoading extends WishListState {}

class WishListStateSuccess extends WishListState {
  String message;
  WishListStateSuccess({this.message});
}

class WishListLoadSuccess extends WishListState {
  List<Datum> banner;
  WishListLoadSuccess({this.banner});
}

class WishListDeleteSuccess extends WishListState {
  List<Datum> banner;
  WishListDeleteSuccess({this.banner});
  // String message;
  // WishListDeleteSuccess({this.message});
}

class WishListLoadFailed extends WishListState {
  String message;
  WishListLoadFailed({this.message});
}

class WishListSavedFailed extends WishListState {
  String message;
  WishListSavedFailed({this.message});
}

class WishListDeleteFailed extends WishListState {
  String message;
  WishListDeleteFailed({this.message});
}
