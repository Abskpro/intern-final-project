import 'package:intern_project/models/login_model.dart';

abstract class LoginState {}

class LoginInitialState extends LoginState {}

class LoginLoadingState extends LoginState {}

class LoginSuccessState extends LoginState {
  Login user;
  LoginSuccessState(this.user);
}

class LoginFailState extends LoginState {
  String message;

  LoginFailState(this.message);
}
