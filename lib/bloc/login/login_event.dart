abstract class LoginEvent {}

class LoginButtonPressed extends LoginEvent {
  String number, password;

  LoginButtonPressed({this.number, this.password});
}
