import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/login/login_event.dart';
import 'package:intern_project/bloc/login/login_state.dart';
import 'package:intern_project/services/repository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginRepository loginRepository;

  LoginBloc({@required LoginRepository loginRepository}) : super(null) {
    this.loginRepository = loginRepository;
  }

  LoginState get initialState => LoginInitialState();

  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoadingState();
      try {
        var user =
            await loginRepository.fetchUser(event.number, event.password);
        yield LoginSuccessState(user);
        print(user);
      } catch (e) {
        yield LoginFailState(e.toString());
      }
    }
  }
}
