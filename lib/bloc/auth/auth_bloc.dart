import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intern_project/bloc/auth/auth_event.dart';
import 'package:intern_project/bloc/auth/auth_state.dart';
import 'package:intern_project/services/repository.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  BannerRepository bannerRepository;
  AuthBloc({BannerRepository bannerRepository}) : super(null) {
    this.bannerRepository = bannerRepository;
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AppStartedEvent) {
      yield UnauthenticatedState();
    }
  }
}
