import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/cart/cart_event.dart';
import 'package:intern_project/bloc/cart/cart_state.dart';
import 'package:intern_project/services/repository.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartRepository cartRepository;

  CartBloc({@required CartRepository cartRepository}) : super(null) {
    this.cartRepository = cartRepository;
  }

  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is CartButtonPressedEvent) {
      yield CartEventLoading();
      try {
        var banner = await cartRepository.updateCart(event.pid, event.quantity);
        yield CartStateSuccess(message: banner);
      } catch (e) {
        yield CartStateFailed();
        print(e.toString());
      }
    }
    if (event is CartLoadEvent) {
      yield CartLoading();
      try {
        var banner = await cartRepository.loadCart();
        yield CartLoadSuccess(banner: banner);
      } catch (e) {
        yield CartLoadFailed();
        print(e.toString());
      }
    }
  }
}
