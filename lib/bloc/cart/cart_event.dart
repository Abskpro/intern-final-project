abstract class CartEvent {}

class CartLoadEvent extends CartEvent {}

class CartButtonPressedEvent extends CartEvent {
  String pid, quantity;
  CartButtonPressedEvent({this.pid, this.quantity});
}
