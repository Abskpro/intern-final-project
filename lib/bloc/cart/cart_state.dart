import 'package:intern_project/models/cart_model.dart';

abstract class CartState {}

class CartStateInitial extends CartState {}

class CartEventLoading extends CartState {}

class CartLoading extends CartState {}

class CartStateSuccess extends CartState {
  String message;
  CartStateSuccess({this.message});
}

class CartLoadSuccess extends CartState {
  List<CartProduct> banner;
  CartLoadSuccess({this.banner});
}

class CartStateFailed extends CartState {
  String message;
  CartStateFailed({this.message});
}

class CartLoadFailed extends CartState {
  String message;
  CartLoadFailed({this.message});
}
