import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intern_project/bloc/brands/brands_event.dart';
import 'package:intern_project/bloc/brands/brands_state.dart';
import 'package:intern_project/services/repository.dart';

class FeaturedBrandBloc extends Bloc<FeaturedBrandsEvent, FeaturedBrandsState> {
  BrandRepository brandRepository;

  FeaturedBrandBloc({@required BrandRepository brandRepository}) : super(null) {
    this.brandRepository = brandRepository;
  }

  FeaturedBrandsStateInitial get initialState => FeaturedBrandsStateInitial();

  Stream<FeaturedBrandsState> mapEventToState(
      FeaturedBrandsEvent event) async* {
    if (event is FeaturedBrandLoad) {
      yield FeaturedBrandsStateLoading();
      try {
        var banner = await brandRepository.fetchBrands();
        yield FeaturedBrandsStateSuccess(banner);
      } catch (e) {
        yield FeaturedBrandsLoadFailed();
        print(e.toString());
      }
    }
  }
}
