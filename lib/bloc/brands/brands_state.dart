import 'package:intern_project/models/brands_model.dart';

abstract class FeaturedBrandsState {}

class FeaturedBrandsStateInitial extends FeaturedBrandsState {}

class FeaturedBrandsStateLoading extends FeaturedBrandsState {}

class FeaturedBrandsStateSuccess extends FeaturedBrandsState {
  final List<Datum> banner;

  FeaturedBrandsStateSuccess(this.banner);
}

class FeaturedBrandsLoadFailed extends FeaturedBrandsState {}
